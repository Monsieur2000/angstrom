function filtreUds() {
    // Declare variables
    var input, filter, grid, li, a, i;
    input = document.getElementById('filtre-input');
    filter = input.value.toUpperCase();
    console.log(filter)
    grid = document.getElementById("result-search");
    section = grid.getElementsByTagName('section');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < section.length; i++) {
        a = section[i].getElementsByTagName("h1")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            section[i].style.display = "";
        } else {
            section[i].style.display = "none";
        }
    }
    $grid.isotope({ sortBy: 'cote' }); // appel du tri isotope pour eviter que les sections restent à leur place.
}
