
$( document ).ready(function() {
var $grid = $('.grid-tri').isotope({
  itemSelector: '.result-item',
  layoutMode: 'vertical',
  vertical: {
    horizontalAlignment: 0.5
  },
  getSortData: {
    cote: '[id]',
    date: '.data-date',
    titre: '.titre-ud'
  }
});

$('.tri-groupe').on( 'click', 'a', function() {
  var sortByValue = $(this).attr('data-sort-by');

  $grid.isotope({ sortBy: sortByValue });

});
});
