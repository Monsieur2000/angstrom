var $grid = $('.grid-tri').isotope({
  itemSelector: '.tri-item',
  layoutMode: 'vertical',
  vertical: {
    horizontalAlignment: 0.5
  },
  getSortData: {
    cote: '[id]',
    date: '.date-tri',
    titre: '.titre-ud'
  }
});




$('#enfants-tri').on( 'click', 'option', function() {
  var sortByValue = $(this).attr('value');

  $grid.isotope({ sortBy: sortByValue });

});
