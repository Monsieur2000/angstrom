<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:x="http://ead3.archivists.org/schema/"
	exclude-result-prefixes="x"
	>

	<!-- A décomenter pour l'édition pour eviter les alertes chiantes de XMLSpy
	<xsl:variable name="nomcote"></xsl:variable>
	<xsl:variable name="baseurl"></xsl:variable>
	-->

	<xsl:variable name="cotefonds"><xsl:value-of select="substring-before($nomcote,'-')"/></xsl:variable>

	<!--
	Variable pour indenter les breadcrumbs. C'est moche mais c'est comme ça que c'est le plus pratique en XSL 1
 	On peut l'enlever si on fait des imbrications ul/li
	-->
	<xsl:variable name="tabs" select="'&gt; &gt; &gt; &gt; &gt; &gt; &gt; &gt; &gt; &gt; '"/>

	<xsl:output method="html"/>

	<xsl:template match="/">
				<xsl:choose>
					<xsl:when test="$nomcote='uds'"><xsl:apply-templates select="x:ead/x:archdesc/x:dsc"/></xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="//x:c[@id=$nomcote]" mode="ancetre"/>
						<xsl:apply-templates select="//x:c[@id=$nomcote]" mode="principal"/>
					</xsl:otherwise>
				</xsl:choose>
	</xsl:template>

	<xsl:template match="x:chronitem">
		<tr>
			<xsl:apply-templates/>
		</tr>
	</xsl:template>

	<xsl:template match="x:datesingle | x:fromdate | x:todate">
	<xsl:choose>
		<xsl:when test="contains(.,'s.d.')">
			<xsl:text>[s.d.]</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="."/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
<xsl:template match="x:datesingle | x:fromdate" mode="standard">
<xsl:choose>
	<xsl:when test="contains(.,'s.d.')">
		<xsl:text>0</xsl:text>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="@standarddate"/>
	</xsl:otherwise>
</xsl:choose>
</xsl:template>
	<xsl:template match="x:daterange">
	<xsl:param name="date_db"><xsl:apply-templates select="x:fromdate"/></xsl:param>
	<xsl:param name="date_fin"><xsl:apply-templates select="x:todate"/></xsl:param>
	<xsl:choose>
		<xsl:when test="$date_db=$date_fin">
			<xsl:value-of select="$date_db"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$date_db"/>
			<xsl:text> - </xsl:text>
			<xsl:value-of select="$date_fin"/>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>
<xsl:template match="x:dateset" >
	<xsl:for-each select="*">

			<xsl:apply-templates select="." />

			<xsl:if test="position() != last()">

				<xsl:text>, </xsl:text>
			</xsl:if>

	</xsl:for-each>
</xsl:template>

	<xsl:template match="x:unitdatestructured">
		<xsl:apply-templates select="x:daterange"/>
		<xsl:apply-templates select="x:datesingle"/>
		<xsl:apply-templates select="x:dateset"/>
	</xsl:template>



	<xsl:template match="x:unitid">
		<xsl:choose>
			<xsl:when test="@label='principale'">
				<p><strong><xsl:apply-templates/></strong></p>
			</xsl:when>
			<xsl:when test="@label='photographie'">
				<p><xsl:apply-templates/> : cote des photographies.</p>
			</xsl:when>
		</xsl:choose>
	</xsl:template>



	<xsl:template match="x:scopecontent">
		<section id="scopecontent" class="titre-gauche">
			<header>
				<h1>Contenu</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:processinfo">
		<section id="processinfo" class="titre-gauche">
			<header>
				<h1>Traitement</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:bioghist">
		<section id="bioghist" class="titre-gauche">
			<header>
				<h1>Notes biographiques</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:otherfindaid">
		<section id="otherfindaid" class="titre-gauche">
			<header>
				<h1>Autres instruments de recherche</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:relatedmaterial">
		<section id="relatedmaterial" class="titre-gauche">
			<header>
				<h1>Autres ressources internes</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:separatedmaterial">
		<section id="separatedmaterial" class="titre-gauche">
			<header>
				<h1>Autres ressources externes</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>

	<xsl:template match="x:p">
		<p><xsl:apply-templates/></p>
	</xsl:template>

<!-- TEMPLATE PRINCIPALE UDS DE PREMIER NIVEAU -->

	<xsl:template match="x:dsc">
		<section id="intitule">
			<header>
				<h1>
					<xsl:value-of select="/x:ead/x:archdesc/x:did/x:unittitle"/>
				</h1>
			</header>
		</section>
		<xsl:apply-templates select="../x:scopecontent"/>
		<section id="enfants">
				<h2>Sous unités (<xsl:value-of select="count(x:c)"/>)</h2>
				<select id="enfants-tri">
					<option value="cote">Tri par cote</option>
					<option value="date">Tri par date</option>
					<option value="titre">Tri par intitulé</option>
				</select>
				<input type="text" id="filtre-input" onkeyup="filtreUds()" placeholder="Filtre plein texte"/>
		</section>
		<div class="grid-tri" id="grid-tri">
			<xsl:apply-templates select="x:c" mode="table"/>
		</div>
	</xsl:template>

<!-- TEMPLATE POUR AFFICHER LES ENFANTS D'UNE UD -->
	<xsl:template match="x:c" mode="table">
		<section id="{@id}" class="titre-gauche tri-item">
			<header>
				<h1><xsl:value-of select="x:did/x:unitid"/></h1>
			</header>
			<div class="corps-section">
				<h2>
					<a class="titre-ud">
						<xsl:attribute name="href"><xsl:value-of select="@id"/></xsl:attribute>
						<xsl:value-of select="x:did/x:unittitle"/>
					</a>
					<span class="sous-titre-ud"> (<xsl:value-of select="x:did//x:physdescstructured[@physdescstructuredtype='materialtype']/x:unittype"/> : <xsl:value-of select="x:did//x:physdescstructured[@physdescstructuredtype='materialtype']/x:quantity"/>)</span>
				</h2>
				<p><xsl:apply-templates select="x:did/x:unitdatestructured"/></p>
				<p class="date-tri">
					<xsl:apply-templates select="x:did/x:unitdatestructured//x:fromdate" mode="standard"/>
					<xsl:apply-templates select="x:did/x:unitdatestructured//x:datesingle" mode="standard"/>
				</p>
			</div>
		</section>
	</xsl:template>

<!-- BREADCRUMBS -->
	<xsl:template match="x:c" mode="ancetre">
		<!-- FIX-ME : modifier pour avoir un système de ul/li imbriquées -->
		<section id="breadcrumbs">
			<p>
				<xsl:text>&gt; </xsl:text><a href="uds"><xsl:value-of select="/x:ead/x:archdesc/x:did/x:unittitle"/></a>
				<xsl:for-each select="ancestor-or-self::x:c">
					<xsl:variable name="nb_ancetres" select="count(ancestor-or-self::x:c)"/>
					<br/><xsl:value-of select="substring($tabs, 1, (number($nb_ancetres)+1)*2)"/> <!-- On multiplie par deux le nombre d'ancètres car il y a deux caractères à afficher. -->
					<xsl:choose>
						<xsl:when test="position()!=last()">
							<a href="{@id}"><xsl:value-of select="x:did/x:unittitle"/></a>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="x:did/x:unittitle"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</p>
		</section>
	</xsl:template>

<!-- TEMPLATE PRINCIPALE D'UNE UD -->
	<xsl:template match="x:c" mode="principal">
		<section id="intitule">
			<header>
				<hgroup>
					<h1><xsl:value-of select="x:did/x:unittitle"/></h1>
				</hgroup>
			</header>
		</section>
		<section class="titre-gauche" id="did">
			<header>
				<h1>Description</h1>
			</header>
			<div class="corps-section">
					<p>
            <xsl:for-each select="x:did/x:unitid">
              <xsl:value-of select="@label"/> : <span style="font-weight:bold;"><xsl:value-of select="."/></span>
              <xsl:if test="position() != last()">
                <xsl:text>, </xsl:text>
              </xsl:if>
            </xsl:for-each>
						<xsl:if test="x:did/x:physloc">
							<xsl:text>, Localisation : </xsl:text>
							<span style="font-weight:bold;">
								<xsl:for-each select="x:did/x:physloc">
									<xsl:value-of select="."/>
									<xsl:if test="position() != last()">
										<xsl:text>, </xsl:text>
					  			</xsl:if>
								</xsl:for-each>
							</span>
						</xsl:if>
						</p>
						<p>Date : <strong><xsl:apply-templates select="x:did//x:unitdatestructured"/></strong></p>
						<p><xsl:apply-templates select="x:did//x:physdescstructured"/></p>
						<xsl:apply-templates select="x:did/x:didnote"/>
						<xsl:for-each select="x:did/x:origination">
							<p><xsl:value-of select="@label"/> : <strong><xsl:value-of select="."/></strong></p>
						</xsl:for-each>
						<xsl:for-each select="x:did/x:materialspec">
							<p><xsl:value-of select="@label"/> : <strong><xsl:value-of select="."/></strong></p>
						</xsl:for-each>
						<xsl:for-each select="x:userestrict">
							<p><xsl:value-of select="@localtype"/> : <strong><xsl:value-of select="x:p"/></strong></p>
						</xsl:for-each>
				<xsl:if test="x:acqinfo//x:chronitem">
				<p><xsl:for-each select="x:acqinfo//x:chronitem">
						<span style="font-weight:bold;"><xsl:value-of select="x:event"/></span>
						<xsl:text> : </xsl:text>
						<xsl:value-of select="x:datesingle"/>
					<xsl:if test="position() != last()">
						<xsl:text>, </xsl:text>
					  </xsl:if>
				</xsl:for-each></p>
			</xsl:if>
			</div>
		</section>

		<xsl:apply-templates select="x:scopecontent"/>
		<xsl:apply-templates select="x:bioghist"/>
		<xsl:apply-templates select="x:processinfo"/>
		<xsl:apply-templates select="x:otherfindaid"/>
		<xsl:apply-templates select="x:relatedmaterial"/>
		<xsl:apply-templates select="x:separatedmaterial"/>

		<xsl:if test="x:c">
			<section id="enfants">
					<h2>Sous unités (<xsl:value-of select="count(x:c)"/>)</h2>
					<select id="enfants-tri">
						<option value="cote">Tri par cote</option>
						<option value="date">Tri par date</option>
						<option value="titre">Tri par intitulé</option>
					</select>
					<input type="text" id="filtre-input" onkeyup="filtreUds()" placeholder="Filtre plein texte"/>
			</section>
			<div class="grid-tri" id="grid-tri">
				<xsl:apply-templates select="x:c" mode="table"/>
			</div>
		</xsl:if>

	</xsl:template>


	<xsl:template match="x:didnote">
		<p>Remarques :<xsl:text> </xsl:text><strong><xsl:apply-templates /></strong></p>
	</xsl:template>

	<xsl:template match="x:daoset">
		<h3 class="pure-u-4-5">Pièces numériques</h3>
					<div class="doc-num">
						<xsl:apply-templates select="x:dao"/>
					</div>
	</xsl:template>

	<xsl:template match="x:dao">
	<a href="http://srvfjme/~fjme/projetxml/data/pieces/image/{$cotefonds}/{@href}.jpg">
	<img alt="image" src="http://srvfjme/~fjme/projetxml/data/pieces/image/{$cotefonds}/{@href}.jpg"/>
	</a>
	</xsl:template>

	<xsl:template match="x:physdescstructured">
	<xsl:value-of select="x:unittype"/>
	<xsl:text> : </xsl:text>
	<span style="font-weight:bold;">
		<xsl:value-of select="x:quantity"/>
	</span>
	<xsl:if test="position() != last()">
				<xsl:text>, </xsl:text>
			</xsl:if>
	</xsl:template>

	<xsl:template match="x:ref[@target!='']">
		<a href="{$baseurl}fonds/{$nomfonds}/{@target}">
			<xsl:value-of select="."/>
		</a>
	</xsl:template>

	<xsl:template match="x:ref[@linkrole='refTranscriptionMd']">
		<a href="{$baseurl}Page/vue/{@href}" target="_blank">
			<xsl:value-of select="."/>
		</a>
	</xsl:template>
	<xsl:template match="x:ref[@linkrole='refTranscriptionPdf']">
		<a href="{$baseurl}{@href}" target="_blank">
			<xsl:value-of select="."/>
		</a>
	</xsl:template>

	<xsl:template match="x:ref[@linkrole='refBiblio']">
		<a href="http://srvfjme/~fjme/projetbiblio/{@href}">
			<xsl:value-of select="."/>
		</a>
	</xsl:template>

	<xsl:template match="x:ref[@linkrole='refFonds']">
		<a href="{$baseurl}fonds/{@href}">
			<xsl:value-of select="."/>
		</a>
	</xsl:template>

	<xsl:template match="x:list[@listtype='unordered']">
		<ul><xsl:apply-templates/></ul>
	</xsl:template>
	<xsl:template match="x:list[@listtype='ordered']">
		<ol><xsl:apply-templates/></ol>
	</xsl:template>
	<xsl:template match="x:item">
		<li><xsl:apply-templates/></li>
	</xsl:template>

</xsl:stylesheet>
