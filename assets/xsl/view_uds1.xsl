<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:x="http://ead3.archivists.org/schema/"
	exclude-result-prefixes="x"
	>

	<!-- A décomenter pour l'édition pour eviter les alertes chiantes de XMLSpy
	<xsl:variable name="nomcote">APM-003-001</xsl:variable>
	<xsl:variable name="urlfonds">APM-003-001</xsl:variable>
	 -->
	<xsl:output method="html"/>

	<xsl:variable name="cotefonds"><xsl:value-of select="substring-before($nomcote,'-')"/></xsl:variable>
	<xsl:variable name="tabs" select="'&gt; &gt; &gt; &gt; &gt; &gt; &gt; &gt; &gt; &gt; '"/>

	<xsl:template match="/">
		<main>
			<article>
				<xsl:choose>
					<xsl:when test="$nomcote='uds'"><xsl:apply-templates select="x:ead/x:archdesc/x:dsc"/></xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="//x:c[@id=$nomcote]" mode="ancetre"/>
						<xsl:apply-templates select="//x:c[@id=$nomcote]" mode="principal"/>
					</xsl:otherwise>
				</xsl:choose>
			</article>
		</main>
	</xsl:template>

	<xsl:template match="x:chronitem">
		<tr>
			<xsl:apply-templates/>
		</tr>
	</xsl:template>

	<xsl:template match="x:datesingle | x:fromdate | x:todate">
	<xsl:choose>
		<xsl:when test="contains(.,'s.d.')">
			<xsl:text>[s.d.]</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="substring(@standarddate,1,4)"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
	<xsl:template match="x:daterange">
	<xsl:param name="date_db"><xsl:apply-templates select="x:fromdate"/></xsl:param>
	<xsl:param name="date_fin"><xsl:apply-templates select="x:todate"/></xsl:param>
	<xsl:choose>
		<xsl:when test="$date_db=$date_fin">
			<xsl:value-of select="$date_db"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$date_db"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$date_fin"/>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>
<xsl:template match="x:dateset" >
	<xsl:for-each select="*">

			<xsl:apply-templates select="." />

			<xsl:if test="position() != last()">

				<xsl:text>, </xsl:text>
			</xsl:if>

	</xsl:for-each>
</xsl:template>

	<xsl:template match="x:unitdatestructured">
		<xsl:apply-templates select="x:daterange"/>
		<xsl:apply-templates select="x:datesingle"/>
		<xsl:apply-templates select="x:dateset"/>
	</xsl:template>



	<xsl:template match="x:unitid">
		<xsl:choose>
			<xsl:when test="@label='principale'">
				<p><strong><xsl:apply-templates/></strong></p>
			</xsl:when>
			<xsl:when test="@label='photographie'">
				<p><xsl:apply-templates/> : cote des photographies.</p>
			</xsl:when>
		</xsl:choose>
	</xsl:template>



	<xsl:template match="x:scopecontent">
		<section id="scopecontent" class="titre-gauche">
			<header>
				<h1>Contenu</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:bioghist">
		<section id="bioghist" class="titre-gauche">
			<header>
				<h1>Notes biographiques</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:otherfindaid">
		<section id="otherfindaid" class="titre-gauche">
			<header>
				<h1>Autres instruments de recherche</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:relatedmaterial">
		<section id="relatedmaterial" class="titre-gauche">
			<header>
				<h1>Autres ressources internes</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:separatedmaterial">
		<section id="separatedmaterial" class="titre-gauche">
			<header>
				<h1>Autres ressources externes</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>

	<xsl:template match="x:p">
		<p><xsl:apply-templates/></p>
	</xsl:template>



	<xsl:template match="x:dsc">
		<section id="intitule">
			<header>
				<h1>
					<xsl:value-of select="/x:ead/x:archdesc/x:did/x:unittitle"/>
				</h1>
			</header>
		</section>
		<section>
			<header>
				<h1>Rubriques</h1>
			</header>
			<div class="corps-section">
				<p><a href="{$urlfonds}/fonds">Fiche Fonds</a></p>
				<table>
					<tbody>
						<xsl:apply-templates select="x:c" mode="table"/>
					</tbody>
				</table>
			</div>
		</section>
	</xsl:template>

	<xsl:template match="x:c" mode="table">
		<tr>
			<td><xsl:value-of select="x:did/x:unitid"/></td>
			<td>
				<a>
					<xsl:attribute name="href"><xsl:value-of select="@id"/></xsl:attribute>
					<xsl:value-of select="x:did/x:unittitle"/>
				</a>
			</td>
			<td><xsl:apply-templates select="x:did/x:unitdatestructured"/></td>
		</tr>
	</xsl:template>

	<xsl:template match="x:c" mode="ancetre">
		<!-- FIX-ME : modifier pour avoir un système de ul/li imbriquées -->
		<section id="breadcrumbs">
			<p>
				<a href="uds"><xsl:value-of select="/x:ead/x:archdesc/x:did/x:unittitle"/></a>
				<xsl:for-each select="ancestor-or-self::x:c">
					<xsl:variable name="nb_ancetres" select="count(ancestor-or-self::x:c)"/>
					<br/><xsl:value-of select="substring($tabs, 1, (number($nb_ancetres)+1)*2)"/> <!-- On multiplie par deux le nombre d'ancètres car il y a deux caractères à afficher. -->
					<xsl:choose>
						<xsl:when test="position()!=last()">
							<a href="{@id}"><xsl:value-of select="x:did/x:unittitle"/></a>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="x:did/x:unittitle"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
			</p>
		</section>
	</xsl:template>


	<xsl:template match="x:c" mode="principal">
		<section id="intitule">
			<header>
				<hgroup>
					<h1><xsl:value-of select="x:did/x:unittitle"/></h1>
					<h2>(<xsl:apply-templates select="x:did/x:unitdatestructured"/>)</h2>
				</hgroup>
			</header>
		</section>

		<xsl:apply-templates select="x:scopecontent"/>
		<xsl:apply-templates select="x:bioghist"/>

		<xsl:if test="x:c">
			<section>
				<header>
					<h1>Sous-Rubriques</h1>
				</header>
				<div class="corps-section">
					<table>
						<!--
						<thead>
							<tr>
								<th>Cote</th>
								<th>Intitulé</th>
								<th>Dates</th>
							</tr>
						</thead>
						-->
						<tbody>
							<xsl:apply-templates select="x:c" mode="table"/>
						</tbody>
					</table>
				</div>
			</section>
		</xsl:if>

		<section class="titre-gauche">
			<header>
				<h1>Description</h1>
			</header>
			<div class="corps-section">
				<p><xsl:apply-templates select="x:did//x:physdescstructured"/></p>
					<p><xsl:value-of select="x:did/x:unitid/@label"/> : <span style="font-weight:bold;"><xsl:value-of select="x:did/x:unitid"/></span>
					, Localisation : <span style="font-weight:bold;">
				<xsl:for-each select="x:did/x:physloc">

						<xsl:value-of select="."/>
					<xsl:if test="position() != last()">
						<xsl:text>, </xsl:text>
					  </xsl:if>

				</xsl:for-each>
				</span>
				</p>
				<xsl:if test="x:acqinfo//x:chronitem">
				<p><xsl:for-each select="x:acqinfo//x:chronitem">

						<span style="font-weight:bold;"><xsl:value-of select="x:event"/></span>
						<xsl:text> : </xsl:text>
						<xsl:value-of select="x:datesingle"/>
					<xsl:if test="position() != last()">
						<xsl:text>, </xsl:text>
					  </xsl:if>

				</xsl:for-each></p>
			</xsl:if>
			</div>
		</section>
<!--
		<section>
			<header>
				<h1>Pèces numériques</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates select="x:did/x:daoset"/>
				<xsl:if test="x:did/x:dao">
					<h3 class="pure-u-4-5">Pièces numériques</h3>
					<div class="doc-num">
						<xsl:apply-templates select="x:did/x:dao"/>
					</div>
				</xsl:if>
			</div>
		</section>
-->
		<xsl:apply-templates select="x:otherfindaid"/>
		<xsl:apply-templates select="x:relatedmaterial"/>
		<xsl:apply-templates select="x:separatedmaterial"/>





	</xsl:template>

	<xsl:template match="x:daoset">
		<h3 class="pure-u-4-5">Pièces numériques</h3>
					<div class="doc-num">
						<xsl:apply-templates select="x:dao"/>
					</div>
	</xsl:template>

	<xsl:template match="x:dao">

	<a href="http://srvfjme/~fjme/projetxml/data/pieces/image/{$cotefonds}/{@href}.jpg">
	<img alt="image" src="http://srvfjme/~fjme/projetxml/data/pieces/image/{$cotefonds}/{@href}.jpg"/>
	</a>
	</xsl:template>

	<xsl:template match="x:physdescstructured">
	<xsl:value-of select="x:unittype"/>
	<xsl:text> : </xsl:text>
	<span style="font-weight:bold;">
		<xsl:value-of select="x:quantity"/>
	</span>
	<xsl:if test="position() != last()">

				<xsl:text>, </xsl:text>
			</xsl:if>
	</xsl:template>

	<xsl:template match="x:ref">
		<a>
			<xsl:choose>
				<xsl:when test="@target">
					<xsl:attribute name="href">
						<xsl:value-of select="$urlfonds"/>
						<xsl:text>/</xsl:text>
						<xsl:value-of select="@target"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="@linkrole='refFonds'">
					<xsl:attribute name="href">
						<xsl:text>http://srvfjme/~fjme/projetxml/inventaire/view/</xsl:text>
						<xsl:value-of select="@href"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="@linkrole='refBiblio'">
					<xsl:attribute name="href">
						<xsl:text>http://srvfjme/~fjme/projetbiblio/</xsl:text>
						<xsl:value-of select="@href"/>
					</xsl:attribute>
				</xsl:when>
			</xsl:choose>
			<xsl:apply-templates/>
		</a>
	</xsl:template>

</xsl:stylesheet>
