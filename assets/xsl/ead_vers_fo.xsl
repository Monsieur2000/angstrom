<?xml version="1.0" encoding="UTF-8"?>
<!-- Feuille de style pour transformation du EAD 3 vers un répertoire avec informations détaillées pour les archivistes -->
<!-- Réalisée par Vincent Bezençon, Fondation Jean Monnet pour l'Europe, 2016 -->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:x="http://ead3.archivists.org/schema/" exclude-result-prefixes="x">
	<xsl:output indent="yes" method="xml" />
	
<xsl:template match="/">

<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

	<fo:layout-master-set>
		<fo:simple-page-master master-name="A4-portrait" 
									page-height="29.7cm" 
									page-width="21cm" 
									margin-top="1cm" 
									margin-bottom="1cm" 
									margin-left="2cm" 
									margin-right="2cm"
									>
			<fo:region-body margin-top="0.4cm" margin-bottom="1.5cm"/>
			<fo:region-before extent="1.5cm" />
			<fo:region-after extent="1.5cm" />
	    </fo:simple-page-master>

	    <fo:simple-page-master master-name="A4-italienne" 
									page-height="21cm" 
									page-width="29.7cm" 
									margin-top="1cm" 
									margin-bottom="1cm" 
									margin-left="2.7cm" 
									margin-right="2cm"
									>
			<fo:region-body margin-top="1.5cm" margin-bottom="1.5cm"/>
			<fo:region-before extent="1.5cm" />
			<fo:region-after extent="1.5cm" />
	    </fo:simple-page-master>
	</fo:layout-master-set>
	
<!-- Page de titre -->
<fo:page-sequence master-reference="A4-portrait" text-align="justify" font-family="CMU Serif, Liberation Serif, Times New Romand, serif" font-size="12pt">
	<fo:static-content flow-name="xsl-region-before">
		<fo:block></fo:block>
	</fo:static-content>

	<fo:static-content flow-name="xsl-region-after">
		<fo:block border-top="0.5pt solid">
		</fo:block>
	</fo:static-content>
		
	<fo:flow flow-name="xsl-region-body">
		<fo:block space-before="2cm" space-after="2cm"  text-align="center">
			<fo:external-graphic content-width="7cm" content-height="scale-to-fit" src="url('../01_Modeles/logo.png')"/>
		</fo:block>
		<fo:block border-top="0.5pt solid" 
						border-bottom="0.5pt solid" 
						font-weight="bold" 
						font-size="30pt" 						
						text-align="center"
						padding="0.2cm">
			<xsl:value-of select="x:ead/x:archdesc/x:did/x:unittitle"/>
		</fo:block>
			
		<fo:block space-before="14cm" text-align="center">
			<xsl:value-of select="format-date(current-date(), '[Mn] [Y0001]', 'fr', (), ())"/>
		</fo:block>
			
	</fo:flow>			
</fo:page-sequence>

<!-- Résumé Fonds -->
<fo:page-sequence master-reference="A4-portrait" text-align="justify" font-family="CMU Serif, Liberation Serif, Times New Romand, serif" font-size="11pt">

		<fo:static-content flow-name="xsl-region-before">
			<fo:block border-bottom="0.5pt solid" text-align="left">
				<xsl:value-of select="archdesc/did/unittitle" />
		    </fo:block>
		</fo:static-content>

		<fo:static-content flow-name="xsl-region-after">
			<fo:block border-top="0.5pt solid" text-align="right"><xsl:text>Page </xsl:text><fo:page-number /></fo:block>
		</fo:static-content>

		<fo:flow flow-name="xsl-region-body">
			<fo:block padding-bottom="0.4cm" padding-top="0.4cm">
				<fo:block text-align="center" font-weight="bold" font-size="14pt">
					<xsl:value-of select="x:ead/x:archdesc/x:did/x:unittitle"/>
				</fo:block>
			</fo:block>
			<fo:table width="17cm" table-layout="fixed" border-collapse="separate" border-separation="3pt">
				<fo:table-column column-width="5cm" />
				<fo:table-column column-width="12cm" text-align="justify"/>
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Cote</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:for-each select="x:ead/x:archdesc/x:did/x:unitid">
								<fo:block><xsl:value-of select="."/><xsl:text> : </xsl:text><xsl:value-of select="@label"/></fo:block>
							</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Intitulé</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block><xsl:value-of select="x:ead/x:archdesc/x:did/x:unittitle"/></fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Dates extrêmes</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block>
								<xsl:value-of select="x:ead/x:archdesc/x:did//x:fromdate"/>
								<xsl:text> - </xsl:text>
								<xsl:value-of select="x:ead/x:archdesc/x:did//x:todate"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Importance matérielle</fo:block>
						</fo:table-cell>
						<fo:table-cell>							
								<xsl:for-each select="x:ead/x:archdesc/x:did//x:physdescstructured">
									<fo:block>
										<xsl:value-of select="x:unittype"/>
										<xsl:text> : </xsl:text>
										<xsl:value-of select="x:quantity"/>
									</fo:block>
								</xsl:for-each>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Description du contenu</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:apply-templates select="x:ead/x:archdesc/x:scopecontent/x:p" mode="resume"/>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Historique de la conservation</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:apply-templates select="x:ead/x:archdesc/x:custodhist/x:p"  mode="resume"/>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Modalités d'entrée</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:apply-templates select="x:ead/x:archdesc/x:acqinfo//x:chronitem"  mode="resume"/>
							<xsl:apply-templates select="x:ead/x:archdesc/x:acqinfo//x:p"  mode="resume"/>
						</fo:table-cell>
					</fo:table-row>					
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Rapport sur le traitement</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:apply-templates select="x:ead/x:archdesc/x:processinfo//x:chronitem"  mode="resume"/>
							<xsl:apply-templates select="x:ead/x:archdesc/x:processinfo//x:p"  mode="resume"/>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Sources complémentaire</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<xsl:apply-templates select="x:ead/x:archdesc/x:relatedmaterial/x:p"  mode="resume"/>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Localisation</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block>
							<xsl:apply-templates select="x:ead/x:archdesc/x:did/x:physloc"  mode="resume"/>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Langues</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block>
							<xsl:for-each select="x:ead/x:archdesc/x:did/x:langmaterial/x:language">
								<xsl:value-of select="." separator=", "/>
								<xsl:if test="position() != last()">
									<xsl:text>, </xsl:text>
							   </xsl:if>
							</xsl:for-each>
							</fo:block>							
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell>
							<fo:block text-align="left" font-weight="bold" font-family="Liberation Sans Narrow, sans serif" font-size="10pt">| Conditions d'accès</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block>
							<xsl:value-of select="x:ead/x:archdesc/x:accessrestrict/x:p"/>
							</fo:block>							
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:flow>
</fo:page-sequence>




<!-- Répertoire -->
<fo:page-sequence master-reference="A4-portrait" text-align="justify" font-family="CMU Serif, Liberation Serif, Times New Romand, serif" font-size="10pt">

		<fo:static-content flow-name="xsl-region-before">
			<fo:block border-bottom="0.5pt solid" text-align="left">
				<xsl:value-of select="archdesc/did/unittitle" />
		    </fo:block>
		</fo:static-content>

		<fo:static-content flow-name="xsl-region-after">
			<fo:block border-top="0.5pt solid" text-align="right"><xsl:text>Page </xsl:text><fo:page-number /></fo:block>
		</fo:static-content>

		<fo:flow flow-name="xsl-region-body">
			<fo:block padding-bottom="0.4cm" padding-top="0.4cm">
				<fo:block text-align="center" font-weight="bold" font-size="14pt">
					<xsl:value-of select="x:ead/x:archdesc/x:did/x:unittitle"/>
				</fo:block>
				
			</fo:block>	
			<xsl:apply-templates select="x:ead/x:archdesc/x:dsc/x:c"/>
			
		</fo:flow>
</fo:page-sequence>
	
</fo:root>
</xsl:template>


<!-- Templates de mise en formes -->

<xsl:template match="x:c">
	<xsl:param name="niveau"><xsl:value-of select="count(ancestor::x:c)"/></xsl:param>
	<fo:block><!-- Block UD avec enfants -->		
		<xsl:choose>
			<xsl:when test="$niveau=0">
				<!-- <xsl:attribute name="page-break-after">always</xsl:attribute> -->
			</xsl:when>			
			<xsl:otherwise>
				<xsl:attribute name="margin-left">1em</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>
		
	
	<fo:block page-break-inside="avoid"><!-- Block UD seule -->
		<xsl:attribute name="padding-bottom">0.5cm</xsl:attribute>
	<fo:block><!-- Block Titre UD -->
		
		<xsl:choose>
			<xsl:when test="$niveau=0">
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:attribute name="font-size">14pt</xsl:attribute>				
			</xsl:when>
			<xsl:when test="$niveau=1">
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:attribute name="font-size">12pt</xsl:attribute>
				<xsl:attribute name="padding-top">0.5cm</xsl:attribute>
			</xsl:when>
			<xsl:when test="$niveau=2">
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:attribute name="font-size">11pt</xsl:attribute>
				<xsl:attribute name="padding-top">0.3cm</xsl:attribute>
			</xsl:when>
			<xsl:otherwise>
				<xsl:attribute name="font-weight">bold</xsl:attribute>
				<xsl:attribute name="padding-top">0.0cm</xsl:attribute>
			</xsl:otherwise>			
		</xsl:choose>
	
		<xsl:apply-templates select="x:did/x:unittitle"/>
		
		<xsl:apply-templates select="x:did/x:unitdatestructured" mode="date_titre"/>
		
	</fo:block><!-- FIN Block Titre UD -->
	
	<!-- Infos majeures SERIF -->	
	
	<xsl:apply-templates select="x:scopecontent"/>
	
	<xsl:apply-templates select="x:bioghist"/>
	
	<xsl:apply-templates select="x:otherfindaid"/>

	<xsl:apply-templates select="x:relatedmaterial"/>
	
	<xsl:apply-templates select="x:separatedmaterial"/>
	
	<!-- Infos mineures SANS SERIF -->	
	<fo:block font-family="Liberation Sans Narrow, sans serif" font-size="10pt">
	
		<fo:block padding-bottom="2pt"><xsl:apply-templates select="x:accessrestrict"/></fo:block>
	
		<fo:block text-align="left">					
			<xsl:apply-templates select="x:did/x:unitdatestructured"/>			
			<xsl:apply-templates select="x:did/x:materialspec"/>
			<xsl:apply-templates select="x:did/x:physdescset/x:physdescstructured"/>
			<xsl:apply-templates select="x:did/x:physdescstructured"/>
		</fo:block>
		
		<fo:block text-align="left">			
			<xsl:call-template name="cotes"/>
			<xsl:if test="x:did/x:physloc">			
				<xsl:call-template name="localisation"/>			
			</xsl:if>					
			<xsl:apply-templates select="x:did/x:daoset"/>
			<xsl:apply-templates select="x:did/x:dao"/>
		</fo:block>
				
		<fo:block text-align="left">
			<xsl:apply-templates select="x:did/x:origination"/>			
		</fo:block>
		
		<fo:block text-align="left">			
			<xsl:apply-templates select="x:acqinfo"/>
		</fo:block>
		
		<fo:block padding-top="2pt"><xsl:apply-templates select="x:controlaccess"/></fo:block>
		
		<fo:block padding-top="2pt"><xsl:apply-templates select="x:processinfo"/></fo:block>
	</fo:block>
	</fo:block><!-- FIN block UD seule -->
	
	<xsl:apply-templates select="x:c"/>
	</fo:block><!-- FIN block UD avec enfants -->
</xsl:template>

<xsl:template match="x:origination">
	<xsl:text>| </xsl:text>
	<xsl:value-of select="@label"/>
	<xsl:text> </xsl:text>
	<fo:inline font-weight="bold">
		<xsl:value-of select="x:*/x:part"/>
	</fo:inline>
	<xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="x:acqinfo">	
	<xsl:text>| </xsl:text>
	<xsl:for-each select=".//x:chronitem">
		<xsl:text></xsl:text>
		<fo:inline font-weight="bold"><xsl:value-of select="x:event"/>
		<xsl:text> : </xsl:text>
		</fo:inline>
		<xsl:value-of select="x:datesingle"/>
		<xsl:if test="position() != last()">
			<xsl:text>, </xsl:text>
	   </xsl:if>
	</xsl:for-each>
</xsl:template>

<xsl:template name="cotes">
<xsl:for-each-group select="x:did/x:unitid" group-by="@label">
	<xsl:text>| </xsl:text>
	<xsl:value-of select="current-grouping-key()"/>
	<xsl:text> </xsl:text>
	<fo:inline font-weight="bold">
		<xsl:value-of select="current-group()" separator=", "/>
	</fo:inline>
	<xsl:text> </xsl:text>
</xsl:for-each-group>
</xsl:template>

<xsl:template name="localisation">
<xsl:text>| Localisation </xsl:text>
	<fo:inline font-weight="bold">
	<xsl:for-each select="x:did/x:physloc">		
		<xsl:value-of select="." separator=", "/>
		<xsl:if test="position() != last()">
			<xsl:text>, </xsl:text>
	   </xsl:if>
	</xsl:for-each>
	</fo:inline>
		<xsl:text> </xsl:text>
</xsl:template>



<xsl:template match="x:daoset">
	<xsl:text>| </xsl:text>
	<xsl:value-of select="@label"/>
	<xsl:text> </xsl:text>
	<fo:inline font-weight="bold">
		<xsl:value-of select="x:dao/@href" separator=", "/>
	</fo:inline>
	<xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="x:physdescstructured">
	<xsl:text>| </xsl:text>	
	<xsl:value-of select="x:unittype"/>
	<xsl:text> </xsl:text>
	<fo:inline font-weight="bold">
		<xsl:value-of select="x:quantity"/>
	</fo:inline>
	<xsl:text> </xsl:text>	
</xsl:template>

<!-- Dates uniquement avec année ("date_titre") -->
<xsl:template match="x:unitdatestructured" mode="date_titre">
	<xsl:text> (</xsl:text>
	<xsl:apply-templates select="x:datesingle" mode="date_titre"/>
	<xsl:apply-templates select="x:daterange" mode="date_titre"/>
	<xsl:apply-templates select="x:dateset" mode="date_titre"/>
	<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="x:dateset" mode="date_titre">
	<xsl:for-each select="*">
		<xsl:choose>
			<xsl:when test="position() = last()">				
				<xsl:apply-templates select="." mode="date_titre"/>				
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates select="." mode="date_titre"/>
				<xsl:text>, </xsl:text>
			</xsl:otherwise>
		</xsl:choose>	
	</xsl:for-each>
</xsl:template>

<xsl:template match="x:daterange" mode="date_titre">
	<xsl:param name="date_db"><xsl:apply-templates select="x:fromdate" mode="date_titre"/></xsl:param>
	<xsl:param name="date_fin"><xsl:apply-templates select="x:todate" mode="date_titre"/></xsl:param>
	<xsl:choose>
		<xsl:when test="$date_db=$date_fin">
			<xsl:value-of select="$date_db"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$date_db"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$date_fin"/>
		</xsl:otherwise>
	</xsl:choose>	
</xsl:template>

<xsl:template match="x:datesingle | x:fromdate | x:todate" mode="date_titre">
	<xsl:choose>
		<xsl:when test="contains(.,'s.d.')">
			<xsl:text>s.d.</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="substring(@standarddate,1,4)"/>
		</xsl:otherwise>
	</xsl:choose>	
</xsl:template>

<!-- Dates format humain (dates exactes)-->
<xsl:template match="x:unitdatestructured">		
	<xsl:text>| Date exacte </xsl:text>	
	<fo:inline font-weight="bold">
		<xsl:apply-templates select="x:datesingle"/>
		<xsl:apply-templates select="x:daterange"/>
		<xsl:apply-templates select="x:dateset"/>
	</fo:inline>
	<xsl:text> </xsl:text>
	
</xsl:template>

<xsl:template match="x:dateset">
	<xsl:for-each select="*">
		<xsl:choose>
			<xsl:when test="position() = last()">
				<xsl:apply-templates/>				
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
				<xsl:text>, </xsl:text>
			</xsl:otherwise>
		</xsl:choose>	
	</xsl:for-each>
</xsl:template>

<xsl:template match="x:daterange">
	<xsl:apply-templates select="x:fromdate"/>
	<xsl:text> — </xsl:text>
	<xsl:apply-templates select="x:todate"/>
</xsl:template>

<xsl:template match="x:datesingle | x:fromdate | x:todate">
	<xsl:value-of select="."/>
</xsl:template>


<xsl:template match="x:scopecontent | x:bioghist | x:otherfindaid | x:relatedmaterial | x:separatedmaterial">
<fo:block font-size="11pt" padding-after="2pt">
	<xsl:apply-templates/>
</fo:block>
</xsl:template>

<xsl:template match="x:processinfo">	
	
	<xsl:apply-templates>
		<xsl:with-param name="etiquette">Note interne</xsl:with-param>
	</xsl:apply-templates>	
</xsl:template>

<xsl:template match="x:accessrestrict">
	
	<xsl:apply-templates>
		<xsl:with-param name="etiquette">Conditions d'accès</xsl:with-param>
	</xsl:apply-templates>	
</xsl:template>

<xsl:template match="x:chronlist">
	<xsl:param name="etiquette"/>
	<xsl:for-each select="x:chronitem">
		<fo:block>
			<xsl:text>| </xsl:text>
			<xsl:value-of select="$etiquette"/>
			
				<xsl:text> [</xsl:text>
				<xsl:value-of select="x:datesingle"/>
				<xsl:text>] </xsl:text>			
			<fo:inline font-weight="bold">
				<xsl:value-of select="x:event"/>
			</fo:inline>
		</fo:block>	
	</xsl:for-each>
</xsl:template>

<xsl:template match="x:p[ancestor::x:accessrestrict] | x:p[ancestor::x:processinfo]">
	<xsl:param name="etiquette"/>
	<fo:block>
			<xsl:text>| </xsl:text>
			<xsl:value-of select="$etiquette"/>
			<xsl:text> </xsl:text>
			<fo:inline font-weight="bold">
				<xsl:value-of select="."/>
			</fo:inline>
		</fo:block>	


</xsl:template>

<xsl:template match="x:unitid | x:materialspec">
	<xsl:choose>
		<xsl:when test="@label='principale'">
			<xsl:text>| Cote </xsl:text>
			<fo:inline font-weight="bold"><xsl:value-of select="."/></fo:inline>
			<xsl:text> </xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>| </xsl:text>
			<xsl:value-of select="@label"/>
			<xsl:text> </xsl:text>
			<fo:inline font-weight="bold"><xsl:value-of select="."/></fo:inline>
			<xsl:text> </xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template match="x:dao">
	<xsl:text>|&#160;</xsl:text>
	<xsl:value-of select="@label"/>
	<xsl:text>&#160;</xsl:text>
	<fo:inline font-weight="bold"><xsl:value-of select="@href"/></fo:inline>
	<xsl:text> </xsl:text>
</xsl:template>


<xsl:template match="x:controlaccess">
	<xsl:apply-templates select="x:occupation"/>
	<xsl:apply-templates select="x:subject"/>
</xsl:template>

<xsl:template match="x:occupation | x:subject">
	<fo:block>
		<xsl:text>| Mot-clef </xsl:text>
		<fo:inline font-weight="bold">
			<xsl:for-each select="x:part">	
				<xsl:choose>
					<xsl:when test="position() = last()">
						<xsl:apply-templates/>				
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates/>
						<xsl:text>, </xsl:text>
					</xsl:otherwise>
				</xsl:choose>	
			</xsl:for-each>
		</fo:inline>
	</fo:block>
</xsl:template>

<xsl:template match="x:physdesc">
				<xsl:text>, </xsl:text><xsl:value-of select="."/><xsl:text>.</xsl:text>
</xsl:template>
<!--
<xsl:template match="@otherlevel">
	<fo:block>
		<fo:inline font-style="italic"><xsl:text>Niveau de description : </xsl:text></fo:inline>
		<xsl:value-of select="."/>
	</fo:block>
</xsl:template>
-->
<xsl:template match="x:langmaterial">
	<fo:block keep-with-previous="always">
		<fo:inline font-style="italic"><xsl:text>Langue(s) : </xsl:text></fo:inline>
		<xsl:value-of select="."/>
	</fo:block>
</xsl:template>

<xsl:template match="x:p[ancestor::x:scopecontent]">
	<fo:block padding-after="1pt">
		<xsl:if test="position()=1">
			<xsl:attribute name="text-indent">1em</xsl:attribute>
		</xsl:if>		
		<xsl:apply-templates/>
	</fo:block>
</xsl:template>

<xsl:template match="x:list">
	<fo:list-block font-weight="normal" font-size="12pt" text-align="justify"
									provisional-distance-between-starts="15mm"
									provisional-label-separation="5mm"
									keep-with-previous="always">
		<xsl:apply-templates/>
	</fo:list-block>
</xsl:template>

<xsl:template match="x:item">
	<fo:list-item>
		<fo:list-item-label start-indent="5mm" end-indent="label-end()">
			<fo:block>−</fo:block>
		</fo:list-item-label>
		<fo:list-item-body start-indent="body-start()">
			<fo:block><xsl:apply-templates/></fo:block>
		</fo:list-item-body>
	</fo:list-item>
</xsl:template>

<xsl:template match="x:list[@type='ordered']/x:item">
	<fo:list-item>
		<fo:list-item-label start-indent="5mm" end-indent="label-end()">
			<fo:block><xsl:number count="item" format="1." /></fo:block>
		</fo:list-item-label>
		<fo:list-item-body start-indent="body-start()">
			<fo:block><xsl:apply-templates/></fo:block>
		</fo:list-item-body>
	</fo:list-item>
</xsl:template>



<!-- <xsl:template match="persname | corpname | geogname | subject">
	<fo:inline font-weight="bold">
		<xsl:value-of select="."/>
	</fo:inline>
</xsl:template>-->

<xsl:template match="x:emph[@render='italic']">
	<fo:inline font-style="italic">
		<xsl:value-of select="."/>
	</fo:inline>
</xsl:template>

<xsl:template match="x:emph[@render='bold']">
	<fo:inline font-weight="bold">
		<xsl:value-of select="."/>
	</fo:inline>
</xsl:template>

<xsl:template match="x:p" mode="resume">
	<fo:block padding-after="1pt">				
		<xsl:apply-templates/>
	</fo:block>
</xsl:template>

<xsl:template match="x:chronitem" mode="resume">
	<fo:block>
		<xsl:value-of select="x:datesingle"/>
		<xsl:text> : </xsl:text>
		<xsl:value-of select="x:event"/>
	</fo:block>
</xsl:template>


</xsl:stylesheet>
