<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:x="http://ead3.archivists.org/schema/"
	exclude-result-prefixes="x"
	>
	<xsl:output method="html"/>
	<!-- A décommenter lors de l'édition pour éviter les alertes qui font chier de XMLSpy.
	<xsl:variable name="nomfonds"/>
	<xsl:variable name="baseurl"/>
	 -->
	<xsl:template match="/">

			<section id="intitule">
				 <header>
					<hgroup>
					<h1><xsl:value-of select="x:ead/x:archdesc/x:did/x:unittitle"/></h1>
					<h2>
						<xsl:text>(</xsl:text>
						<xsl:value-of select="x:ead/x:archdesc/x:did/x:unitdatestructured/x:daterange/x:fromdate"/>
						<xsl:text>-</xsl:text>
						<xsl:value-of select="x:ead/x:archdesc/x:did/x:unitdatestructured/x:daterange/x:todate"/>
						<xsl:text>)</xsl:text>
					</h2>
					</hgroup>
				</header>
			</section><!-- /card #intitule -->

			<section id="cote">
				 <header>
					<h1>Cotes - Localisation</h1>
				</header>
				<div class="corps-section">
					<p>
						<strong>Cotes : </strong>
						<xsl:for-each select="x:ead/x:archdesc/x:did/x:unitid">
							<xsl:value-of select="."/>
							<xsl:if test="position() != last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					</p>
					<p>
						<strong>Localisation : </strong>
						<xsl:for-each select="x:ead/x:archdesc/x:did/x:physloc">
							<xsl:value-of select="."/>
								<xsl:if test="position() != last()">
								<xsl:text>, </xsl:text>
							 </xsl:if>
						</xsl:for-each>
					</p>
				</div>
			</section><!-- /card #cote -->

			<section id="imp-mat">
				 <header>
					<h1>Importance matérielle</h1>
				</header>
				<div class="corps-section">
					<p>
						<xsl:for-each select="x:ead/x:archdesc/x:did//x:physdescstructured">
							<xsl:value-of select="x:quantity"/>
							<xsl:text> </xsl:text>
							<xsl:value-of select="x:unittype"/>
							<xsl:if test="position() != last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					</p>
				</div>
			</section><!-- /card #imp-mat -->

			<xsl:apply-templates select="x:ead/x:archdesc/x:bioghist"/>
			<xsl:apply-templates select="x:ead/x:archdesc/x:scopecontent"/>

			<section id="langues">
				 <header>
					<h1>Langues des documents</h1>
				</header>
				<div class="corps-section">
					<p>
						<xsl:for-each select="x:ead/x:archdesc/x:did/x:langmaterial/x:language">
							<xsl:value-of select="."/>
							<xsl:if test="position() != last()">
								<xsl:text>, </xsl:text>
							</xsl:if>
						</xsl:for-each>
					</p>
				</div>
			</section>

				<section id="findaid">
					<header>
						<h1>Inventaires</h1>
					</header>
					<div class="corps-section">
						<xsl:apply-templates select="/x:ead/x:archdesc/x:dsc"/>
						<xsl:apply-templates select="x:ead/x:archdesc/x:otherfindaid"/>
						<xsl:apply-templates select="x:ead/x:control/x:representation"/>
						<xsl:apply-templates select="x:ead/x:control/x:recordid[@instanceurl]"/>
					</div>
				</section>


			<xsl:apply-templates select="x:ead/x:archdesc/x:custodhist"/>
			<xsl:apply-templates select="x:ead/x:archdesc/x:acqinfo"/>
			<xsl:apply-templates select="x:ead/x:archdesc/x:processinfo"/>
			<xsl:apply-templates select="x:ead/x:archdesc/x:separatedmaterial"/>
			<xsl:apply-templates select="x:ead/x:archdesc/x:relatedmaterial"/>
			<xsl:apply-templates select="x:ead/x:archdesc/x:accessrestrict"/>
			<xsl:apply-templates select="x:ead/x:archdesc/x:bibliography"/>


	</xsl:template>
	<xsl:template match="x:recordid">
		<p><a href="../data/fonds_ead/{@instanceurl}" target="_blank">Inventaire en version XML/EAD3</a></p>
	</xsl:template>
	<xsl:template match="x:representation[@linkrole='application/pdf']">
		<p><a href="../data/fonds_pdf/{@href}" target="_blank"><xsl:value-of select="."/></a></p>
	</xsl:template>

	<xsl:template match="x:chronitem">
		<tr>
			<xsl:apply-templates/>
		</tr>
	</xsl:template>
	<xsl:template match="x:datesingle | x:event">
		<td>
			<xsl:value-of select="."/>
		</td>
	</xsl:template>
	<xsl:template match="x:daterange">
		<td>
			<xsl:value-of select="x:fromdate"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="x:todate"/>
		</td>
	</xsl:template>
	<xsl:template match="x:unitdatestructured">

				<h2>Dates</h2>
				<p>
					<xsl:value-of select="x:daterange/x:fromdate"/>
					<xsl:text>-</xsl:text>
					<xsl:value-of select="x:daterange/x:todate"/>
				</p>

	</xsl:template>
	<xsl:template match="x:acqinfo">

				<h2>Modalités d'entrée</h2>
				<xsl:apply-templates/>


	</xsl:template>
	<xsl:template match="x:unitid">
		<xsl:choose>
			<xsl:when test="@label='principale'">
				<p>
					<strong>
						<xsl:apply-templates/>
					</strong>
				</p>
			</xsl:when>
			<xsl:when test="@label='photographie'">
				<p>
					<xsl:apply-templates/> : cote des photographies.</p>
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="x:scopecontent">
		<section id="scopecontent">
			<header>
				<h1>Contenu</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
				<p><xsl:apply-templates select="/x:ead/x:archdesc/x:dsc"/></p>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:custodhist">
		<section id="custodhist">
			<header>
				<h1>Historique de la conservation</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:acqinfo">
		<section id="acqinfo">
			<header>
				<h1>Acquisition</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:relatedmaterial">
		<section id="separatedmaterial">
			<header>
				<h1>Sources complémentaires</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:p">
		<p>
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	<xsl:template match="x:processinfo">
		<section id="processinfo">
			<header>
				<h1>Traitement</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
				<xsl:apply-templates select="../x:appraisal"/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:bioghist">
		<section id="bioghist">
			<header>
				<h1>Biographie du producteur</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:chronlist">
		<table>
			<tbody>
				<xsl:apply-templates select="x:chronitem"/>
			</tbody>
		</table>
	</xsl:template>
	<xsl:template match="x:appraisal">
		<p>
			<xsl:apply-templates/>
		</p>
	</xsl:template>
	<xsl:template match="x:accessrestrict">
		<section id="accessrestrict">
			<header>
				<h1>Restriction d'accès</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:separatedmaterial">
		<section id="separatedmaterial">
			<header>
				<h1>Sources externes</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:bibliography">
		<section id="bibliography">
			<header>
				<h1>Bibliographie</h1>
			</header>
			<div class="corps-section">
				<xsl:apply-templates/>
			</div>
		</section>
	</xsl:template>
	<xsl:template match="x:otherfindaid">
		<xsl:apply-templates/>
	</xsl:template>


	<xsl:template match="x:archref">
		<p>
			<xsl:apply-templates/>
		</p>
	</xsl:template>

	<xsl:template match="x:ref[@linkrole='refInventaire']">
		<xsl:choose>
			<xsl:when test="@show='new'">
				<a href="../{@href}" target="_blank">
					<xsl:value-of select="."/>
				</a>
			</xsl:when>
			<xsl:when test="@show='embed'">
				<p><xsl:value-of select="."/> :</p>
				<embed src="../{@href}"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>



	<xsl:template match="x:ref[@target!='']">
		<a href="{$baseurl}fonds/{$nomfonds}/{@target}"><xsl:value-of select="."/></a>
	</xsl:template>

	<xsl:template match="x:ref[@linkrole='refFonds']">
	  <a>
			<xsl:attribute name="href"><xsl:value-of select="$baseurl"/><xsl:text>fonds/</xsl:text><xsl:value-of select="@href"/></xsl:attribute>

			<xsl:value-of select="."/>
		</a>
	</xsl:template>
<xsl:template match="x:ref[@linkrole='externe']">
  <a href="{@href}" target="_blank"><xsl:value-of select="."/></a>
</xsl:template>

	<xsl:template match="x:emph[@render='italic']">
	  <em><xsl:apply-templates/></em>
	</xsl:template>

	<xsl:template match="x:dsc">
		<param name="nomfonds"/>
			<p>
					<a>
						<xsl:attribute name="href"><xsl:value-of select="$baseurl"/><xsl:text>fonds/</xsl:text><xsl:value-of select="$nomfonds"/><xsl:text>/uds</xsl:text></xsl:attribute>
						<xsl:text>Naviguer dans le fonds</xsl:text>
					</a>
			</p>
	</xsl:template>

	<xsl:template match="x:list[@listtype='unordered']">
		<ul><xsl:apply-templates/></ul>
	</xsl:template>
	<xsl:template match="x:list[@listtype='ordered']">
		<ol><xsl:apply-templates/></ol>
	</xsl:template>
	<xsl:template match="x:item">
		<li><xsl:apply-templates/></li>
	</xsl:template>
</xsl:stylesheet>
