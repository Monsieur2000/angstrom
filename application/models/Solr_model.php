<?php
class Solr_model extends CI_Model {

        public function __construct()
        {
			parent::__construct();

            $this->options = array
						(
						    'hostname' => 'srvfjme.unil.ch',
						    'port'     => 8983,
						    'path'     => '/solr/fjme-inventaires',
						);

        }


	public function get_infos($requete,$filtres=NULL,$plage=array(0,50))
	{

		$client = new SolrClient($this->options);
		$query = new SolrQuery();
    //$param = new SolrParams();
    //$param->setParam('q.op', 'AND')

		$requete = explode(' ',$requete);

		$texte = array();

		foreach ($requete as $ligne) {
			if(0 === strpos($ligne, 'fd:')) {
				$ligne = substr($ligne, 3);
				$query->addFilterQuery("refInventaire:$ligne");

			} elseif (0 === strpos($ligne, 'dt:')) {
        $ligne = substr($ligne, 3);
        $length = strlen($ligne)-1;
        if(strpos($ligne,'-') === $length) $date = '['.substr($ligne, 0, $length).' TO *]';
        elseif(strpos($ligne,'-') === 0) $date = '[* TO '.substr($ligne, -($length)).']';
        elseif(strpos($ligne,'-') === FALSE) $date = $ligne.'*';
        else {
          $ligne = explode('-',$ligne);
          $date = '['.$ligne[0].' TO '.$ligne[1].']';
        }
        $query->addFilterQuery("standarddate:$date");
        // echo("<script>console.log('date: ".$date."');</script>");
			} else {
				$texte[] = $ligne;
			}
		}

    //if(isset($filtres['fonds'])) $query->addFilterQuery("fonds:{$filtres['fonds']}");
    if($filtres ==! NULL){
      foreach($filtres as $champs => $filtre)

      $query->addFilterQuery($champs.':"'.$filtre.'"');

    }

		$texte = implode(' ',$texte);

		$query->setQuery($texte);
		$query->setStart($plage[0]);
		$query->setRows($plage[1]);
    $query->setParam('q.op', 'AND');
		// champs à retourner
		$query->addField('id')
				->addField('refInventaire')
				->addField('fonds')
				->addField('cote')
				->addField('intitule')
				->addField('humandate')
				->addField('standarddate')
				->addField('sourceType')
				->addField('auteur');

    // facetting
    $query->setFacet(true);
    $query->addFacetField('vedette')
            ->addFacetField('sourceType')
            ->setFacetMinCount(1);

		$updateResponse = $client->query($query);

		//return $updateResponse->getRequestUrl();
		return $updateResponse->getResponse();

		}

}

?>
