<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Docphys_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->table = 'doc_phys';
  }

  public function new_lines($data)
  {
    $this->db->insert_batch($this->table, $data);
  }

  public function get_docs_phys($data)
  {
    return $this->db->get_where($this->table, $data)->result();
  }
}

?>
