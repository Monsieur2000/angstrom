<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Docnum_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
    $this->table = 'doc_num';
  }

  public function new_lines($data)
  {
    $this->db->insert_batch($this->table, $data);
  }

  public function new_line($data)
  {
    $this->db->insert($this->table, $data);
  }

  public function test_exist($where=NULL,$like=NULL)
  {
    if (!is_null($where)) $this->db->like($where);
    if (!is_null($like)) $this->db->like($like);

    $query = $this->db->get($this->table);

    if ($query->num_rows() > 0) return TRUE;
    else return FALSE;

  }

  public function get_docs_num($data)
  {
    $results = $this->db->order_by('mime, filename')
                        ->get_where($this->table, $data)
                        ->result();

    $videos = $audios = $textes = $images = NULL;

    foreach ($results as $doc) {
      if($doc->video_codec == 'avc1') {
        list($fonds) = explode('-',$doc->cote,2);
        $chemin = "video/$fonds/$doc->filename";
        $videos[] = array('chemin' => $chemin, 'mime' => $doc->mime);

      } elseif ($doc->extension == 'mp3') {
        list($fonds) = explode('-',$doc->cote,2);
        $chemin = "audio/$fonds/$doc->filename";
        $audios[] = array('chemin' => $chemin, 'mime' => $doc->mime);

      } elseif ($doc->extension == 'pdf' && strpos($doc->chemin,'diffusion') !== FALSE) {
        list($fonds) = explode('-',$doc->cote,2);
        $chemin = "image/$fonds/$doc->filename";
        $textes[] = array('chemin' => $chemin, 'mime' => $doc->mime);

      } elseif ($doc->extension == 'jpg' && strpos($doc->chemin,'diffusion') !== FALSE) {
        list($fonds) = explode('-',$doc->cote,2);
        $chemin = "image/$fonds/$doc->filename";
        $images[] = array('chemin' => $chemin, 'mime' => $doc->mime);

      }
    }

    // docs contient tous les résultats, les autres contienennt les documents à afficher
    return array('docs' => $results,
                  'videos' => $videos,
                  'audios' => $audios,
                  'textes' => $textes,
                  'images' => $images
                  );

  }

  public function liste_chemins($fonds) {
    return $this->db->select('id, chemin, filename')
                    ->like('chemin', $fonds, 'both')
                    ->get($this->table)->result();
  }

  public function del_line($id) {
    $this->db->where('id', $id)->delete($this->table);
  }

  public function fetch_cotes_loc($cotes_fonds,$loc) {
  	$this->db->select('cote')->distinct();
  	foreach($cotes_fonds as $key => $value) {
  		if($key == 0) {
  			$this->db->like('cote', $value.'-', 'after');
  		} else {
  			$this->db->or_like('cote', $value.'-', 'after');
  		}
  	}
  	$this->db->where('localisation', $loc);
  	$query = $this->db->get($this->table)->result();
    $data = array();
    if(count($query) > 0) {
      foreach($query as $row){
        $data[] = $row->cote;
      }
    }
    return $data;
  }

  

}

?>
