<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->model('Docnum_model');

  }

  public function index()
  {
    $this->etat_fonds();
  }

  public function fetch_videos_metadata($video)
  {
      $data = shell_exec("exiftool -json -api largefilesupport=1 $video");

      $data = json_decode($data, TRUE);
      $data = $data[0];
      $cote = $taille = $unite = $codec_video = $codec_audio = NULL;
      $champs_req = array ();
      // Récup cote sur la base du nom de fichier
      list($cote) = explode('.',$data['FileName'],2);
      list($cote) = explode('_',$cote,2);
      // Récup nombre pour la taille
      list($taille, $unite) = explode(' ',$data['FileSize']); // normallement la taille est toujours en MB mais au cas où, penser à faire un check ici, sur $unite

      // check codec vidéo
      if (isset($data['CompressorID'])) $codec_video = $data['CompressorID'];
      elseif (isset($data['VideoCodecID'])) $codec_video = $data['VideoCodecID'];
      elseif(isset($data['VideoFormat']))  $codec_video = $data['VideoFormat'];
      else $codec_video = '[indéterminé]';
      // check codec audio
      if (isset($data['AudioFormat'])) $codec_audio  = $data['AudioFormat'];
      elseif ($data['FileType'] == 'DV') $codec_audio  = 'PCM';
      else $codec_audio = '[indéterminé]';

      $localisation = NULL;
      $localisation = $this->extract_loc($data['SourceFile']);

      $champs_req = array (
          'cote' => $cote,
          'chemin' => $data['SourceFile'],
          'filename' => $data['FileName'],
          'taille_mb' => $taille,
          'extension' => $data['FileTypeExtension'],
          'mime' => $data['MIMEType'],
          'duree' => $data['Duration'],
          'taille_px' => $data['ImageSize'],
          'audio_codec' => $codec_audio,
          'audio_canaux' => $data['AudioChannels'],
          'video_codec' => $codec_video
        );

      $this->Docnum_model->new_line($champs_req);
      echo $champs_req['chemin'].' : entré<br/>';
  }

  public function fetch_audio_metadata($son)
  {
      $data = shell_exec("exiftool -json -api largefilesupport=1 $son");

      $data = json_decode($data, TRUE);
      $data = $data[0];

      $cote = $taille = $unite = $codec_audio = $canaux = $duree = NULL;
      $champs_req = array ();
      // Récup cote sur la base du nom de fichier
      list($cote) = explode('.', $data['FileName'], 2);
      list($cote) = explode('_', $cote, 2);
      // Récup nombre pour la taille
      list($taille, $unite) = explode(' ', $data['FileSize']); // normallement la taille est toujours en MB mais au cas où, penser à faire un check ici, sur $unite
      // retrait des "(aprox)" dans durée
      list($duree) = explode(' ', $data['Duration'], 2);

      // check codec audio FIXME : revoir le code en fonction des formats audio
      if ($data['FileTypeExtension'] == 'mp3') {
        $codec_audio  = 'MPEG-'.$data['MPEGAudioVersion'].' Audio Layer '.$data['AudioLayer'];
      } elseif ($data['FileTypeExtension'] == 'wav') {
	       $codec_audio = $data['Encoding'];
	      }
      // check cannaux
      if (isset($data['NumChannels'])) $canaux = $data['NumChannels'];
      elseif(strpos($data['ChannelMode'], 'Stereo') !== FALSE) $canaux = 2;
      elseif(strpos($data['ChannelMode'], 'Single') !== FALSE) $canaux = 1;

      $localisation = NULL;
      $localisation = $this->extract_loc($data['SourceFile']);

      $champs_req = array (
          'cote' => $cote,
          'chemin' => $data['SourceFile'],
          'filename' => $data['FileName'],
          'taille_mb' => $taille,
          'extension' => $data['FileTypeExtension'],
          'mime' => $data['MIMEType'],
          'duree' => $duree,
          'audio_codec' => $codec_audio,
          'audio_canaux' => $canaux
      );



        $this->Docnum_model->new_line($champs_req);
        echo $champs_req['chemin'].' : entré<br/>';
  }

  public function fetch_images_metadata($image)
  {
    $data = shell_exec("exiftool -json -api largefilesupport=1 $image");

    $data = json_decode($data, TRUE);
    $data = $data[0];

    $cote = $taille_mb = $unite = $taille_px = $resolution = NULL;
    $champs_req = array ();

    // Récup cote sur la base du nom de fichier
    list($cote) = explode('.', $data['FileName'], 2);
    list($cote) = explode('_', $cote, 2);
    // Récup nombre pour la taille
    list($taille_mb, $unite) = explode(' ', $data['FileSize']);

    if(strtoupper($unite) == 'KB') $taille_mb = $taille_mb/1000;

    if($data['FileTypeExtension'] == 'pdf') {
      $taille_px = $resolution = NULL;
    } else {
      $taille_px = $data['ImageSize'];
      $resolution = $data['XResolution'];
    }

    $localisation = NULL;
    $localisation = $this->extract_loc($data['SourceFile']);

    $champs_req = array (
      'cote' => $cote,
      'chemin' => $data['SourceFile'],
      'filename' => $data['FileName'],
      'taille_mb' => $taille_mb,
      'extension' => $data['FileTypeExtension'],
      'mime' => $data['MIMEType'],
      'taille_px' => $taille_px,
      'resolution' => $resolution,
      'localisation' => $localisation
    );

    $this->Docnum_model->new_line($champs_req);
    echo $champs_req['chemin'].' : entré<br/>';
  }

  public function fetch_metadata_from_fonds($fonds) {

    $chemins = ["/mnt/nas_archives/pieces/diffusion",
                "/mnt/nas_archives/pieces/mezzanine",
                "/mnt/nas_archives/pieces/preservation"
              ];

// ATTENTION ! les extensions .ivr (RealPlayer) donnent des métadonnées erronées avec exiftool. Ce script ne les prends pas en compte. De plus, l'extension vaut parfois pour de l'audio parfois pour de la vidéo.

    foreach ($chemins as $chemin) {
      $videos = glob("$chemin/video/$fonds/*.{mov,mkv,mp4,dv}", GLOB_BRACE); // ! ne pas mettre d'espace entre les virgules du glob
      $sons = glob("$chemin/audio/$fonds/*.{mp3,aiff,wav}", GLOB_BRACE);
      $images = glob("$chemin/image/$fonds/*.{jpg,pdf,tif}", GLOB_BRACE);

      foreach ($videos as $video) {
        if($this->Docnum_model->test_exist(array('chemin' => $video)) === FALSE) {
          $this->fetch_videos_metadata($video);
        } else {
          echo $video.' : exsiste déjà<br/>';
        }
      }
      foreach ($sons as $son) {
        if($this->Docnum_model->test_exist(array('chemin'=> $son)) === FALSE) {
          $this->fetch_audio_metadata($son);
        } else {
          echo $son.' : exsiste déjà<br/>';
        }
      }
      foreach ($images as $image) {
        if($this->Docnum_model->test_exist(array('chemin' => $image)) === FALSE) {
          $this->fetch_images_metadata($image);
        } else {
          echo $image.' : exsiste déjà<br/>';
        }
      }
    }
  }

  public function etat_fonds()
  {
    $chemin = '/mnt/nas_archives/inventaires/ead';
		$filtre = array('xml');

		$listresult = array();

		$fichiers =  new DirectoryIterator($chemin);

		foreach($fichiers as $fichier)
		{
			if(in_array($fichier->getExtension(),$filtre))
			{
        $xml = file_get_contents($fichier->getPathname());
        $ead = new SimpleXMLElement($xml);
        $ead->registerXPathNamespace('x', 'http://ead3.archivists.org/schema/');
        $control = $ead->xpath("/x:ead/x:control");
        $cotes = $ead->xpath("/x:ead/x:archdesc/x:did/x:unitid");

        $fonds = (string)$control[0]->recordid;

        foreach ($control[0]->localcontrol as $localcontrol) {
            $localtype = (string)$localcontrol['localtype'];
            //var_dump($localcontrol);
            $listresult[$fonds][$localtype] = (string)$localcontrol->term;
        }

				$listresult[$fonds]['titre'] = (string)$control[0]->filedesc->titlestmt->titleproper;

        foreach ($cotes as $cote) {
          $listresult[$fonds]['cote'][] = (string)$cote;
        }
			}
		}

    $data['infos'] = $listresult;

    $this->load->view('gestion/etat_fonds', $data);
  }

  public function pieces_manquantes($fonds=NULL) {
    $chemin = '/mnt/nas_archives/inventaires/ead';
    $listeuds = array();

    $ead = simplexml_load_file("$chemin/$fonds.ead.xml");
    $ead->registerXPathNamespace('x', 'http://ead3.archivists.org/schema/');

    $uds = $ead->xpath('//x:c[not(x:c)]');
    $control = $ead->xpath("/x:ead/x:control");
    foreach($ead->xpath("/x:ead/x:archdesc/x:did/x:unitid") as $unitid) {
      $cotes[] = (string)$unitid;
    };

    $listediffusion = $this->Docnum_model->fetch_cotes_loc($cotes,'diffusion');
    $listemezzanine = $this->Docnum_model->fetch_cotes_loc($cotes,'mezzanine');
    $listepreservation = $this->Docnum_model->fetch_cotes_loc($cotes,'preservation');

    foreach ($uds as $ud) {
      if(!in_array((string)$ud['id'],$listediffusion)){
          $listeuds[(string)$ud['id']] = array("titre" => (string)$ud->did->unittitle, "cote" => (string)$ud->did->unitid[0]);
          if(in_array((string)$ud['id'],$listemezzanine)) $listeuds[(string)$ud['id']]['mezzanine'] = TRUE;
          if(in_array((string)$ud['id'],$listepreservation)) $listeuds[(string)$ud['id']]['preservation'] = TRUE;

      }
    }




    $data['titre_fonds'] = (string)$control[0]->filedesc->titlestmt->titleproper;
    $data['uds'] = $listeuds;
    $data['nb'] = count($listeuds);
    $data['radical'] = $fonds;

    //echo '<pre>' . var_export($listeuds, true) . '</pre>';

    $this->load->view('gestion/fonds_sans_piece_num', $data);
    // $this->output->enable_profiler(TRUE);

  }

  public function clean_db($fonds=NULL,$del=NULL) {
    clearstatcache(TRUE);
    if ($fonds == NULL) die("Indiquer un fonds");
    $liste_file = $this->Docnum_model->liste_chemins($fonds);

    foreach ($liste_file as $file) {
      if(file_exists($file->chemin)) {
          echo $file->filename." exsiste</br>";

      } else {
        echo $file->filename." inexistant</br>";
        // if($del == 'del') {
        //   $this->Docnum_model->del_line($file->id);
        //   echo $file->filename." ligne effacée</br>";
        // }
      }
    }
    $this->output->enable_profiler(TRUE);

    // echo '<pre>';
    // print_r($listresult);
    // echo'</pre>';
  }


  function extract_loc($chemin) {

    if (strpos($chemin, 'diffusion') !== false) {
      return 'diffusion';
    } elseif (strpos($chemin, 'mezzanine') !== false) {
      return 'mezzanine';
    } elseif (strpos($chemin, 'preservation') !== false) {
      return 'preservation';
    }
  }


  public function piece($cote) {
    $data['docs'] = $this->Docnum_model->get_docs_num(array('cote' => $cote));
    $data['cote'] = $cote;

    $this->load->view('gestion/piece',$data);

  }

  public function srvdoc($id) {
    $doc = $this->Docnum_model->get_docs_num(array('id' => $id));

    $path = $doc['docs'][0]->chemin;
    header('Content-Type: ' . $doc['docs'][0]->mime);
    header('Content-Disposition: attachment; filename="'.$doc['docs'][0]->filename.'"');
    header('Content-Length: ' . filesize($path));
    readfile($path);

  }

}
