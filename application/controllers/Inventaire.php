<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventaire extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

   	$this->load->helper('file');
   	$this->load->library('user_agent');
   	$this->load->helper('url');

   	$this->load->model('Solr_model', 'searchEngine');
   	$this->load->model('Inventaires_model');
		$this->load->model('Docnum_model');
		$this->load->model('Docphys_model');
	}

	public function index()
	{
		$data['titre_page'] = "Inventaires";
		$this->load->view('start', $data);

	}

	public function view($fichier,$etage="fonds")
	{
		if (!strpos($fichier, '.ead')) $fichier = $fichier.'.ead';

		$ead = new DOMDocument();
		$ead->load($this->chemin_ead.'/'.$fichier.'.xml');

		$xsl = new DOMDocument();

		if($etage == "fonds") {
			$xsl->load(FCPATH.'assets/xsl/view_fonds.xsl');
		} else {
			$xsl->load(FCPATH.'assets/xsl/view_uds.xsl');
			$data['radical'] = $fichier; // pour affichage lien vers la "fiche fonds" dans la navigation latérale
		}

		$proc = new XSLTProcessor();
		$proc->importStyleSheet($xsl);
		$proc->setParameter('', 'nomfonds', $fichier);
		$proc->setParameter('', 'nomcote', $etage);
		$proc->setParameter('', 'baseurl', base_url());

		$vue = $proc->transformToXML($ead);

		$data['docs_num'] = $this->Docnum_model->get_docs_num(array ('cote' => $etage));
		$data['docs_phys'] = $this->Docphys_model->get_docs_phys(array ('cote' => $etage));

		$data['vue'] = $vue;
		$data['titre_page'] = $ead->getElementsByTagName('archdesc')->item(0)
									->getElementsByTagName('did')->item(0)
									->getElementsByTagName('unittitle')->item(0)
									->nodeValue;

		$data['script_js'] = array('jquery.min.js','isotope.pkgd.js','isotope-tri-uds.js','filtre-uds.js','no-right-click.js');

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('vue_direct');
		$this->load->view('foot');

		//$this->output->enable_profiler(TRUE);
	}

	public function pdf($fichier) {


	}


	public function plancompactus($nom)
	{
		$ead = new DOMDocument();
		$ead->load(FCPATH.'data/info/occupation.xml');

		$xsl = new DOMDocument();
		$xsl->load(FCPATH.'assets/xsl/view_'.$nom.'.xsl');

		$proc = new XSLTProcessor();
		$proc->importStyleSheet($xsl);

		$vue = $proc->transformToXML($ead);

		switch ($nom) {
			case 'CD' :
				$data['titre_page'] = 'Compactus de Droite';
				break;
			case 'CG' :
				$data['titre_page'] = 'Compactus de Gauche';
				break;
		}

		$data['vue'] = $vue;

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('test_vue');
		$this->load->view('foot');
	}

	public function listfonds()
	{
		$champs = 'cote, intitule, radical, etat';
		$listfonds = $this->Inventaires_model->get_liste();

		$data['listfonds'] = $listfonds;
		$data['titre_page'] = 'Etat des fonds';
		$data['script_js'] = array('jquery.min.js','isotope.pkgd.js','isotope-tri-uds.js','filtre-uds.js', 'post-search.js');
		//$data['nav_tri'] = $this->load->view('nav/nav-tri-filtre-uds', '', TRUE); //partie de tri et filtre injecté ensuite dans le header

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('listefonds_cards');
		$this->load->view('foot');
	}

	public function plan()
	{
		$data['titre_page'] = 'Plan';

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('plan');
		$this->load->view('foot');
	}

	public function vuedao($fichier)
	{
		$arr = explode("-", $fichier, 2);
		$fonds = $arr[0];

		$data['titre_page'] = 'Plan';
		$data['fonds'] = $fonds;
		$data['fichier'] = $fichier;

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('image');
		$this->load->view('foot');
	}

	public function search()
	{
		if (NULL == $this->input->post('q')) {
			$data['titre_page'] = "Inventaires";
			$this->load->view('start', $data);
		} else {
		$requete = trim($this->input->post('q'));
		$filtres = array();


		// Calcul de la fourchette de résultat à afficher en fonction du numéro de la page
		// la plage s'exprime ainis : (ligne de départ, nombre de résultats à afficher)
		if($this->input->post('page') ==! NULL) {
			$page = $this->input->post('page');
			$plage_nb_results = array((50*($page-1)),50);
		} else {
			$page = 1;
			$plage_nb_results = array(0,50);
		}

		// Ajout des filtres de facettes
		if($this->input->post('vedette') ==! NULL) {
			$filtres['vedette'] = $this->input->post('vedette');
		}
		if($this->input->post('sourceType') ==! NULL) {
			$filtres['sourceType'] = $this->input->post('sourceType');
		}
		// Requête
		$result = $this->searchEngine->get_infos($requete,$filtres,$plage_nb_results);

		// Pagination
		$data_pagination['nb_pages'] = ceil($result->response->numFound/50);
		$data_pagination['page'] = $page;
		$data['pagination'] = $this->load->view('pagination', $data_pagination, TRUE);

		$data['titre_page'] = 'Résultat de la recherche';
		$data['filtres'] = $filtres;
		$data['result'] = $result;
		$data['search'] = htmlspecialchars($requete);
		$data['script_js'] = array('jquery.min.js','isotope.pkgd.js','isotope-result-search.js', 'filtre-results.js', 'post-search.js');
		//$data['nav_tri'] = $this->load->view('nav/nav-tri-filtre-uds', '', TRUE); //partie de tri et filtre injecté ensuite dans le header NE PAS OUBLIER LES SCRIPTS JS QUI VONT AVEC

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('search_view');
		$this->load->view('foot');
	}
	}


	public function requete1($requete=NULL)
	{
		if(is_null($requete) && isset($_POST['search'])) $requete = $_POST['search'];

		echo $requete;

		$chemin = $this->chemin_ead;

		$filtre = array('xml');

		$listresult = array();

		$fichiers =  new DirectoryIterator($chemin);

		foreach($fichiers as $fichier)
		{
			if(in_array($fichier->getExtension(),$filtre))
			{
				$ead = simplexml_load_file($fichier->getPathname());

				$items = $ead->xpath("//c[contains(did/unittitle,'".$requete."')]|//c[contains(scopecontent,'".$requete."')]");

				foreach ($items as $item) {
					$listresult[] = array(
									'fonds' => $fichier->getBasename('.xml'),
									'cote'  => $item->did->unitid,
									'titre' => $item->did->unittitle,
									'contenu' => $item->scopecontent
								);
				}
			}
		}

		$data['result'] = $listresult ;
		$data['titre_page'] = "Résultats" ;

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('recherche');
		$this->load->view('foot');
	}

	public function requete($requete=NULL)
	{
		if(is_null($requete) && isset($_POST['search'])) $requete = $_POST['search'];

		$chemin = $this->chemin_ead;

		$filtre = array('xml');

		$listresult = array();

		$fichiers =  new DirectoryIterator($chemin);

		$nbresult = 0;

		foreach($fichiers as $fichier)
		{
			if(in_array($fichier->getExtension(),$filtre))
			{
				$ead = new DOMDocument;
				$ead->load($fichier->getPathname());

				$fonds = $ead->getElementsByTagName('archdesc')->item(0)
								->getElementsByTagName('did')->item(0)
								->getElementsByTagName('unittitle')->item(0)
								->nodeValue;

				$xPath = new DOMXPath($ead);
				$xPath->registerNamespace("php", "http://php.net/xpath");
				$xPath->registerPHPFunctions();

				$q = "//c[php:functionString('stripos', did/unittitle, ' $requete')]|
						//c[php:functionString('stripos', scopecontent, ' $requete')]|
						archdesc[php:functionString('stripos', //*, ' $requete')]
					";
				//$q = "//c[php:functionString('stripos', //*[not(name()='c')], ' $requete')]";

				$items = $xPath->query($q);

				foreach ($items as $item) {

				$nbresult++;

				if($item->getElementsByTagName('datesingle')->length !=0) $dates = $item->getElementsByTagName('datesingle')->item(0)->nodeValue;
				if($item->getElementsByTagName('daterange')->length !=0) $dates = $item->getElementsByTagName('fromdate')->item(0)->nodeValue.'-'.$item->getElementsByTagName('todate')->item(0)->nodeValue;

				if($item->getElementsByTagName('scopecontent')->length !=0) $contenu = $item->getElementsByTagName('scopecontent')->item(0)->nodeValue;
				else $contenu = "";

				$listresult[$fonds][] = array(
									'fichier' => $fichier->getBasename('.xml'),
									'cote'  => $item->getElementsByTagName('did')->item(0)->getElementsByTagName('unitid')->item(0)->nodeValue,
									'cote_url' => $item->getAttribute('id'),
									'titre' => $item->getElementsByTagName('did')->item(0)->getElementsByTagName('unittitle')->item(0)->nodeValue,
									'contenu' => $contenu,
									'dates' => $dates
								);
				}
			}
		}

		$data['result'] = $listresult ;
		$data['nbresult'] = $nbresult ;
		$data['titre_page'] = "Résultats" ;

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('recherche');
		$this->load->view('foot');
	}

}
?>
