<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

    include APPPATH . 'third_party/Parsedown.php';

   	$this->load->helper('file');
   	$this->load->library('user_agent');
   	$this->load->helper('url');

	}

  public function index()
	{


	}

  public function vue($dossier,$sousdossier,$doc_md=NULL)
	{

    $file = FCPATH."data/$dossier/$sousdossier/$doc_md.md";
		
    $data['titre_page'] = file($file)[0];

		$data['script_js'] = array('jquery.min.js');
    $parseur = new Parsedown();
    $data['contenu'] = $parseur->text(file_get_contents($file));

		$this->load->view('head', $data);
		$this->load->view('header');
		$this->load->view('vue_page');
		$this->load->view('foot');

	}

}
