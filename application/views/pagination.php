<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="page-section">
  <p>
    <!-- If the current page is more than 1, show the First and Previous links -->
<?php if($page > 1) : ?>
    <label>
      <input type="radio" name="page" value='1'/>
      <span><i class="fa fa-fast-backward" aria-hidden="true"></i></span>
    </label>
    <label>
      <input type="radio" name="page" value='<?php echo ($page - 1); ?>'/>
      <span><i class="fa fa-step-backward" aria-hidden="true"></i></span>
    </label>
<?php endif; ?>

<?php
    //setup starting point

    //$max is equal to number of links shown
    $max = 7;
    if($page < $max)
        $sp = 1;
    elseif($page >= ($nb_pages - floor($max / 2)) )
        $sp = $nb_pages - $max + 1;
    elseif($page >= $max)
        $sp = $page  - floor($max/2);
?>

<!-- If the current page >= $max then show link to 1st page -->
<?php if($page >= $max) : ?>
  <label>
    <input type="radio" name="page" value='1'/>
    <span>1</span>
  </label>
  <span>..</span>
<?php endif; ?>

<!-- Loop though max number of pages shown and show links either side equal to $max / 2 -->
<?php for($i = $sp; $i <= ($sp + $max -1);$i++) : ?>
    <?php
        if($i > $nb_pages)
            continue;
    ?>

    <?php if($page == $i) : ?>
        <strong><?php echo $i; ?></strong>
    <?php else : ?>
      <label>
        <input type="radio" name="page" value='<?php echo $i; ?>'/>
        <span><?php echo $i; ?></span>
      </label>
    <?php endif; ?>
<?php endfor; ?>


<!-- If the current page is less than say the last page minus $max pages divided by 2-->
<?php if($page < ($nb_pages - floor($max / 2))) : ?>
    <span>..</span>
    <label>
      <input type="radio" name="page" value='<?php echo $nb_pages; ?>'/>
      <span><?php echo $nb_pages; ?></span>
    </label>
<?php endif; ?>

<!-- Show last two pages if we're not near them -->
<?php if($page < $nb_pages) : ?>
  <label>
    <input type="radio" name="page" value='<?php echo ($page + 1); ?>'/>
    <span><i class="fa fa-step-forward" aria-hidden="true"></i></span>
  </label>
    <label>
      <input type="radio" name="page" value='<?php echo $nb_pages; ?>'/>
      <span><i class="fa fa-fast-forward" aria-hidden="true"></i></span>
    </label>
<?php endif; ?>
  </p>
</section>
