<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title><?php echo $titre_page; ?></title>

		<!-- Polices -->
		<link href="https://fonts.googleapis.com/css?family=Habibi|Work+Sans" rel="stylesheet">

		<!-- CSS -->
		<link rel="stylesheet" href="<?php echo base_url("assets/css/reset.css"); ?>" type="text/css" media="screen,projection">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/font-awesome.min.css"); ?>" type="text/css" media="screen,projection">
    <link rel="stylesheet" href="<?php echo base_url("assets/css/start.css"); ?>" type="text/css" media="screen,projection">

    </head>

    <body>

<main>


			<img src="<?php echo base_url("assets/img/logo_grand_bleu.jpg"); ?>"/>

			<h2 style="padding-top:1em;">Archives de la Fondation</h2>

			<form style="padding-top:1em;" action="<?php echo base_url("Inventaire/search/");?>" method="post">
				<input id="search" name="q" type="search" placeholder="Recherche" value =""/>
			</form>

			<p style="padding-top:1em;">
				<a href="<?php echo base_url("fonds");?>">Etat des fonds</a>
			</p>



</main>
</body>
</html>
