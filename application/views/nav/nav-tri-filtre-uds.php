<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section id="filtre">
  <header>
    <h1>Filtre</h1>
  </header>
  <input type="text" onkeyup="filtreUds()" placeholder="Filtre plein texte" placeholder="Texte à filtrer"></input>
</section>
<section id="tri">
  <header>
    <h1>Tri</h1>
  </header>
  <ul class="tri-groupe">
    <li><a href="#" data-sort-by="cote">Cote</a></li>
    <li><a href="#" data-sort-by="date">Date</a></li>
    <li><a href="#" data-sort-by="titre">Intitulé</a></li>
  </ul>
</section>
