<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<header id="page-header">
	<div id="header-logo">
		<a href="<?php echo base_url();?>">
		<img src="<?php echo base_url("assets/img/logo_txt_blanc.png");?>" />
		</a>
	</div>
	<h1>Consultation des archives</h1>
</header>
<form id="search-form" action="<?php echo base_url("Inventaire/search/");?>" method="post">
<nav>

	<section id="search">
		<div class="embed-submit-button">
			<input class="search-field" id="search-field" name="q" type="search" placeholder="Rechercher" value ="<?php if(isset($search)) echo $search; ?>"/>
			<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
		</div>
			<a id="info-search" href="<?php echo base_url("Page/vue/dossiers/guides/GuideRecherche");?>"><i class="fa fa-info fa-2x" aria-hidden="true"></i></a>
	</section>
	<section id="menu">
		<header>
			<h1>Navigation</h1>
		</header>
		<ul>
			<li><a href="<?php echo site_url('fonds');?>">Liste des Fonds</a></li>
			<?php if (isset($radical)): ?>
			<li><a href="<?php echo site_url('fonds').'/'.$radical;?>">Détails du fonds courant</a></li>
		<?php endif ?>
		</ul>
	</section>
	<?php if(isset($nav_tri)) echo $nav_tri; ?>
	<?php if(isset($result->facet_counts->facet_fields->vedette)): ?>
	<section class="facette-section">
		<header>
	    <h1>Fonds (<?php echo count((array)$result->facet_counts->facet_fields->vedette) ?>)</h1>
	  </header>
	  <ul class="facette">
			<?php foreach ($result->facet_counts->facet_fields->vedette as $nom => $nb): ?>
	    <li>
				<label>
					<input type="checkbox" name="vedette" value="<?php echo $nom; ?>" <?php if(isset($filtres['vedette'])) echo 'checked'; ?>/>
					<span><?php echo $nom.' : '.$nb; ?></span>
				</label>
			</li>
		<?php endforeach; ?>
		<?php echo("<script>console.log('date: ".count((array)$result->facet_counts->facet_fields->vedette)."');</script>"); ?>
	  </ul>
	</section>
<?php endif; ?>
<?php if(isset($result->facet_counts->facet_fields->sourceType)): ?>
<section class="facette-section">
	<header>
		<h1>Types (<?php echo count((array)$result->facet_counts->facet_fields->sourceType) ?>)</h1>
	</header>
	<ul class="facette">
		<?php foreach ($result->facet_counts->facet_fields->sourceType as $nom => $nb): ?>
		<li>
			<label>
				<input type="checkbox" name="sourceType" value="<?php echo $nom; ?>" <?php if(isset($filtres['sourceType'])) echo 'checked'; ?>/>
				<span><?php echo $nom.' : '.$nb; ?></span>
			</label>
		</li>
	<?php endforeach; ?>
	</ul>
</section>
<?php endif; ?>

</nav>
