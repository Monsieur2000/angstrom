<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
	<article>
		<section id="intitule">
			<header>
				<h1>
					<?php echo $titre_page;	?>
				</h1>
			</header>
		</section>
		<section>
			<?php if ($result->response->numFound == 0): ?>
			<p>La recherche n'a donné aucun résultat.</p>
			<?php if(isset($search)) echo "<p><strong>Mots-clefs : </strong>$search</p>"; ?>
			<?php if(isset($filtres['vedette'])) echo "<p><strong>Fonds : </strong>".$filtres['vedette']."</p>"; ?>
			<?php if(isset($filtres['sourceType'])) echo "<p><strong>Type de documents : </strong>".$filtres['sourceType']."</p>"; ?>
			<?php else: ?>
			<p>La recherche a donné <strong><?php echo $result->response->numFound; ?></strong> résultats.</p>
			<?php if(isset($search)) echo "<p><strong>Mots-clefs : </strong>$search</p>"; ?>
			<?php if(isset($filtres['vedette'])) echo "<p><strong>Fonds : </strong>".$filtres['vedette']."</p>"; ?>
			<?php if(isset($filtres['sourceType'])) echo "<p><strong>Type de documents : </strong>".$filtres['sourceType']."</p>"; ?>
			<?php endif ?>

		</section>
		<?php if ($result->response->numFound > 0): ?>
		<div id="result-search" class="grid-tri">
			<?php foreach($result->response->docs as $doc): ?>
					<section class="result-item" id="<?php echo $doc->cote[0]; ?>">
<?php switch($doc->sourceType[0]): // ces deux lignes en doivent pas être indentées pour que le switch fonctionne ?>
<?php case "fondsArchives": ?>
							<a href="<?php echo base_url("fonds/").$doc->refInventaire[0];?>">
								<img class="vignette" src="<?php echo base_url("assets/img/search/fonds.jpg"); ?>"/>
								<?php break; ?>
								<?php case "fonds": ?>
								<a href="<?php echo base_url("fonds/").$doc->refInventaire[0];?>">
									<img class="vignette" src="<?php echo base_url("assets/img/search/fonds.jpg"); ?>"/>
								<?php break; ?>
							<?php case "archive": ?>
							<a href="<?php echo base_url("fonds/").$doc->refInventaire[0].'/'.$doc->id;?>">
								<img class="vignette" src="<?php echo base_url("assets/img/search/archive.jpg"); ?>"/>
								<?php break; ?>
							<?php case "document": ?>
							<a href="<?php echo base_url("fonds/").$doc->refInventaire[0].'/'.$doc->id;?>">
								<img class="vignette" src="<?php echo base_url("assets/img/search/archive.jpg"); ?>"/>
								<?php break; ?>
							<?php case "Audio": ?>
							<a href="<?php echo base_url("fonds/").$doc->refInventaire[0].'/'.$doc->id;?>">
								<img class="vignette" src="<?php echo base_url("assets/img/search/audio.jpg"); ?>"/>
								<?php break; ?>
							<?php case "Vidéo": ?>
							<a href="<?php echo base_url("fonds/").$doc->refInventaire[0].'/'.$doc->id;?>">
								<img class="vignette" src="<?php echo base_url("assets/img/search/video.jpg"); ?>"/>
								<?php break; ?>
							<?php case "Objet": ?>
							<a href="<?php echo base_url("fonds/").$doc->refInventaire[0].'/'.$doc->id;?>">
								<img class="vignette" src="<?php echo base_url("assets/img/search/objet.jpg"); ?>"/>
								<?php break; ?>
							<?php case "dossier": ?>
							<a href="<?php echo base_url("fonds/").$doc->refInventaire[0].'/'.$doc->id;?>">
								<img class="vignette" src="<?php echo base_url("assets/img/search/dossier.jpg"); ?>"/>
								<?php break; ?>
								<?php case "livre": ?>
								<a href="http://srvfjme/~fjme/projetbiblio/Biblio/livre/<?php echo $doc->id;?>">
									<img class="vignette" src="<?php echo base_url("assets/img/search/livre.jpg"); ?>"/>
									<?php break; ?>
							<?php case "Photographie": ?>
							<a href="<?php echo base_url("fonds/").$doc->refInventaire[0].'/'.$doc->id;?>">
								<?php $photopath = FCPATH."data/pieces/image/".explode("-",$doc->id)[0]."/".$doc->id.".jpg"; ?>
								<?php if(file_exists($photopath)): ?>
								<img class="vignette" src="<?php echo base_url('data/pieces/image/').explode("-",$doc->id)[0]."/".$doc->id.".jpg"; ?>"/>
								<?php else: ?>
								<img class="vignette" src="<?php echo base_url("assets/img/search/photo.jpg"); ?>"/>
							<?php endif; ?>
								<?php break; ?>
							<?php endswitch; ?>
								<div class="droite">
								<header>
									<h1 class="titre-ud"><?php echo $doc->intitule[0]; ?></h1>
								</header>
								<p><?php echo $doc->fonds[0];?><?php if(isset($doc->cote[0])) echo " (".$doc->cote[0].")"; ?></p>
								<p>
								<?php if(isset($doc->humandate)) echo $doc->humandate[0]; ?>
								<?php if(isset($doc->auteur)):
												foreach($doc->auteur as $auteurname):
													   echo ', '.$auteurname;
												endforeach;
								endif;?>
								</p>
								<p class="data-date"><?php if(isset($doc->standarddate)) echo $doc->standarddate[0]; ?></p>
							</div>
							</a>
					</section>
				<?php endforeach; ?>
			<?php endif; ?>
			</div>

		</section>
		<?php echo $pagination; ?>

	</article>
</main>
