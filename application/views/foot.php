<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
</form>


		<!-- JS -->
		<?php if(isset($script_js)): ?>
      <?php foreach ($script_js as $row): ?>
        <script type="text/javascript" src="<?php echo base_url('assets/js').'/'.$row; ?>"></script>
      <?php endforeach; ?>
    <?php endif; ?>
<script>
//if( !(navigator.userAgent.toLowerCase().indexOf('firefox') > -1) ){
//	document.body.innerHTML = "<main><h1>Ce site ne fonction qu'avec Mozilla Firefox</h1><p>Vous pouvez le télécharger <a href=\"https://www.mozilla.org/fr/firefox/new/\">ici</a>.</p></main>";
// }
$('#search-field').focus( function() {
  $('#search').animate({width: "420px"}, 400);
});
$('#search-field').blur( function() {
  $('#search').animate({width: "300px"}, 400);
});
</script>
	</body>
</html>
