<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
	<article>
		<section id="intitule">
			<header>
				<h1>
					<?php echo $titre_page;	?>
				</h1>
			</header>
		</section>
		<section id="enfants">
				<h2></h2>
				<select id="enfants-tri">
					<option value="cote">Tri par cote</option>
					<option value="date">Tri par date</option>
					<option value="titre">Tri par intitulé</option>
				</select>
				<input type="text" id="filtre-input" onkeyup="filtreUds()" placeholder="Filtre plein texte"/>
		</section>
		<div id="grid-tri" class="grid-tri">
		<?php foreach($listfonds as $fonds): ?>
		<section id="<?php echo $fonds->cote; ?>" class="titre-gauche tri-item">
			<header>
				<h1><?php echo $fonds->cote; ?></h1>
			</header>
			<div class="corps-section">
				<h2><a class="titre-ud" href="<?php echo site_url('fonds').'/'.$fonds->radical; ?>"><?php echo $fonds->vedette; ?></a></h2>
				<p><strong>Etat :</strong> <?php echo $fonds->etat; ?></p>
			</div>
		</section>
	<?php endforeach; ?>
		</div>
	</article>
</main>
