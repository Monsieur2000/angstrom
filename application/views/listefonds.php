<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main>
	<article>
		<section id="listfonds">
		 <table>
			 <thead>
				<tr>
					<th data-field="titre">Cote</th>
					<th data-field="titre">Intitulé</th>
					<th data-field="etat">Etat du traitement</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($listfonds as $fonds): ?>
				<tr>
					<td><?php echo $fonds->cote; ?></td>
					<td><a href="<?php echo site_url('fonds').'/'.$fonds->radical; ?>"><?php echo $fonds->intitule; ?></a></td>
					<td><?php echo $fonds->etat; ?></td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		</section>
		</article>
<script type="text/javascript">
      $(document).ready(function()
		{
        $("#listfonds").tablesorter();
        $("#listfonds").filterTable({inputSelector: '#filtre'});
		}
	  );
      </script>


</main>
