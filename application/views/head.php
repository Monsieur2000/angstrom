<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

		<title><?php echo $titre_page; ?></title>

		<!-- Polices -->
		<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville|Work+Sans" rel="stylesheet">

		<!-- CSS -->
		<link rel="stylesheet" href="<?php echo base_url("assets/css/reset.css"); ?>" type="text/css" media="screen,projection">    
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url("assets/css/style.css"); ?>" type="text/css" media="screen,projection">

    </head>

    <body>
