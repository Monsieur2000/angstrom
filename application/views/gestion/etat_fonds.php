<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>FJME - Etats des inventaires</title>
		<link rel="stylesheet" href="<?php echo base_url("assets/css/tacit-css.min.css");?>"/>
	</head>
	<body>
		<header>
			<h1>Etat des inventaires</h1>
		</header>
		<main>
			<article>
				<table id="infofonds">
          <tr>
            <th onclick="sortTable(0)">Vedette</th>
            <th onclick="sortTable(1)">Intitulé</th>
            <th onclick="sortTable(2)">Cotes</th>
            <th onclick="sortTable(3)">Etat</th>
            <th onclick="sortTable(4)">Niveau de détail</th>
            <th onclick="sortTable(5)">Format de l'inventaire</th>
            <th onclick="sortTable(6)">Microfilms</th>
            <th onclick="sortTable(7)">Numérisation</th>
          </tr>
          <?php foreach ($infos as $radical => $fonds): ?>
          <tr>
            <td><?php if(isset($fonds['vedette'])) echo $fonds['vedette']; ?></td>
            <td><a href="<?php echo base_url('fonds/').$radical;?>"><?php if(isset($fonds['titre']))echo $fonds['titre']; ?></a></td>
            <td><?php if(isset($fonds['cote']))echo implode(', ',$fonds['cote']); ?></td>
            <td><?php if(isset($fonds['etatinventaire']))echo $fonds['etatinventaire']; ?></td>
            <td><?php if(isset($fonds['niveaudedetail']))echo $fonds['niveaudedetail']; ?></td>
            <td><?php if(isset($fonds['typeinventaire']))echo $fonds['typeinventaire']; ?></td>
            <td><?php if(isset($fonds['microfilm']))echo $fonds['microfilm']; ?></td>
            <td><a href="<?php echo base_url('gestion/pieces_manquantes/').$radical;?>"><?php if(isset($fonds['numerisation']))echo $fonds['numerisation']; ?></a></td>
          </tr>
        <?php endforeach; ?>
        </table>
			</article>
		</main>
    <script>
function sortTable(n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = document.getElementById("infofonds");
  switching = true;
  //Set the sorting direction to ascending:
  dir = "asc";
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n];
      y = rows[i + 1].getElementsByTagName("TD")[n];
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
</script>
	</body>
</html>
