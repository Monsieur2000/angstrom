<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>

<html lang="fr">
<head>
  <meta charset="utf-8">

  <title>Interface de gestion</title>
  <meta name="description" content="Interface de gestion">
  <meta name="author" content="Vincent">

  <!-- <link rel="stylesheet" href="css/styles.css?v=1.0"> -->
</head>

<body>
  <header><h1>Interface de Gestion</h1></header>
  <main>
    <ul>
      <li><a href="<?php echo site_url("Gestion/fetch_videos_metadata"); ?>">Récupération et injecttion des métadonnées Vidéos</a></li>
      <li><a href="<?php echo site_url("Gestion/fetch_audio_metadata"); ?>">Récupération et injecttion des métadonnées Audios</a></li>
      <li><a href="<?php echo site_url("Gestion/fetch_images_metadata"); ?>">Récupération et injecttion des métadonnées Images</a></li>
    </ul>
  </main>
</body>
</html>
