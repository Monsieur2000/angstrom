<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>FJME - <?php echo $cote; ?> : liste des versions numériques</title>
		<link rel="stylesheet" href="<?php echo base_url("assets/css/tacit-css.min.css");?>"/>
	</head>
	<body>
		<header>
			<h1><?php echo $cote; ?> : liste des versions numériques</h1>
		</header>
		<main>
			<article>
				<?php if (! is_null($docs['docs'])): ?>
          <ul>
          <?php foreach ($docs['docs'] as $doc): ?>
            <li><a href="<?php echo base_url('Gestion/srvdoc/').$doc->id; ?>"><?php echo $doc->filename; ?></a></li>
          <?php endforeach; ?>
          </ul>
        <?php else: ?>
          <p>Pas de pièce numérique présente.</p>
        <?php endif; ?>
			</article>
		</main>
    <script></script>
	</body>
</html>
