<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<main>
		<article>

			<?php print $vue; ?>
			<?php if (! is_null($docs_num['videos'])): ?>
				<section id="video" class="titre-gauche">
					<header>
						<h1>A regarder</h1>
					</header>
					<div class="corps-section">
						<?php foreach ($docs_num['videos'] as $video): ?>
						<video controls>
							<source src="<?php echo base_url('data/pieces').'/'.$video['chemin']; ?>" type="<?php echo $video['mime']; ?>"/>
						</video>
						<?php endforeach; ?>
					</div>
				</section>
			<?php endif; ?>
			<?php if (! is_null($docs_num['audios'])): ?>
				<section id="audio" class="titre-gauche">
					<header>
						<h1>A écouter</h1>
					</header>
					<div class="corps-section">
						<?php foreach ($docs_num['audios'] as $audio): ?>
						<audio controls>
							<source src="<?php echo base_url('data/pieces').'/'.$audio['chemin']; ?>" type="<?php echo $audio['mime']; ?>"/>
						</audio>
						<?php endforeach; ?>
					</div>
				</section>
			<?php endif; ?>
			<?php if (! is_null($docs_num['textes'])): ?>
				<section id="textes" class="titre-gauche">
					<header>
						<h1>A lire</h1>
					</header>
					<div class="corps-section">
						<?php foreach ($docs_num['textes'] as $texte): ?>
						<embed src="<?php echo base_url('data/pieces').'/'.$texte['chemin']; ?>" type="<?php echo $texte['mime']; ?>">
						</embed>
						<?php endforeach; ?>
					</div>
				</section>
			<?php endif; ?>
			<?php if (! is_null($docs_num['images'])): ?>
				<section id="images" class="titre-gauche">
					<header>
						<h1>A Regarder</h1>
					</header>
					<div class="corps-section">
						<?php foreach ($docs_num['images'] as $image): ?>
						<img src="<?php echo base_url('data/pieces').'/'.$image['chemin']; ?>"/>
						<?php endforeach; ?>
					</div>
				</section>
			<?php endif; ?>
			<?php if(! empty($docs_phys)): ?>
			<section id="doc_phys" class="titre-gauche">
				<header>
					<h1>Exemplaires physiques</h1>
				</header>
				<div class="corps-section">
					<?php foreach ($docs_phys as $doc): ?>
					<p>
						<?php echo $doc->format; ?> (<?php echo $doc->type; ?>) ·
						<strong><?php echo $doc->qte; ?> <?php echo $doc->support; ?></strong> ·
						localisation : <strong><?php echo "$doc->loc.$doc->cote"; ?></strong>
						<?php if(! is_null($doc->notes)) echo "· $doc->notes"; ?>
					</p>
					<?php endforeach; ?>
				</div>
			</section>
			<?php endif; ?>

			<?php if(! empty($docs_num['docs'])): ?>
			<section id="doc_nums" class="titre-gauche">
				<header>
					<h1>Exemplaires numériques</h1>
				</header>
				<div class="corps-section">
					<?php foreach ($docs_num['docs'] as $doc): ?>
					<p>
						<?php echo $doc->mime ?> ·
						<strong><?php echo str_replace("/mnt/nas_archives/pieces/","",$doc->chemin); ?></strong>

								<?php if(! is_null($doc->video_codec)) echo " · codec vidéo : $doc->video_codec" ?>
								<?php if(! is_null($doc->audio_codec)) echo " · codec audio : $doc->audio_codec" ?>
								<?php if(! is_null($doc->taille_px)) echo " · Taille : $doc->taille_px"."px" ?>
								<?php if(! is_null($doc->resolution)) echo " · resolution : $doc->resolution dpi" ?>
								<?php if(! is_null($doc->notes)) echo " · remarques : $doc->notes" ?>

					</p>
					<?php endforeach; ?>
				</div>
			</section>

			<?php endif; ?>
		</article>
	</main>
